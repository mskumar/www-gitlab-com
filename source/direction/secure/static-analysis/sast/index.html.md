---
layout: markdown_page
title: "Category Direction - Static Applicaton Security Testing (SAST)"
---

- TOC
{:toc}

## Description

### Overview

Static application security testing (SAST) checks source code to find possible security vulnerabilities. SAST helps developers identify weaknesses and security issues earlier in the software development lifecycle, before code is deployed. SAST usually is performed when code is being submitted to a code repository. Think of it as a spell checker for security issues.

SAST is performed on source code or binary files and thus usually wont require code to be compiled, built, or deployed. However, this means that SAST cannot detect runtime or environment issues. SAST can analyze the control flow, the abstract syntax tree, how functions are invoked, and if there are information leaks in order to detect weak points that may lead to unintended behaviors.

Just like spell checkers, SAST analyzers are language and syntax specific and can only identify known classes of issues. SAST does not replace code reviewers, instead, it augments them, and provides another line of proactive defense against common and known classes of security issues. SAST is specifically about identifying potential security issues, so it should not be mistaken for [Code Quality](https://about.gitlab.com/direction/verify/code_quality/).

Security tools like SAST are best when integrated directly into the [Devops Lifecycle](https://about.gitlab.com/stages-devops-lifecycle/) and every project can benefit from SAST scans, which is why we include it in [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

### Goal

Overall we want to help developers write better code and worry less about common security mistakes. SAST should help *prevent* security vulnerabilities by helping developers easily *identify* common security issues as code is being contributed and *mitigate* proactively.  SAST should *integrate* seamlessly into a developer’s workflow because security tools that are actively used are effective.

* *Prevent* - find common security issues as code is being contributed and before it gets deployed to production.
* *Identify* - continuously monitor source code for known or common issues.
* *Mitigate* - make it easy to remediate identified issues, automatically if possible.
* *Integrate* - integrate with the rest of the devops pipeline and [play nice with other vendors](https://about.gitlab.com/handbook/product/#plays-well-with-others).

We also want to make SAST complexity totally transparent to users. GitLab is able to automatically detect the programming language and to run the proper analyzer. We want to increase language coverage by including support for the most common languages.

Language priorities (in addition to our existing [language support](https://docs.gitlab.com/ee/user/application_security/sast/#supported-languages-and-frameworks))
- *Java*
- *C#* (.Net Framework + Core)
- *PHP*
- *JavaScript* (better support for popular frameworks)
- Python
- Go
- Kotlin (Android)
- Swift (Apple)

*User success metrics*

The following measures would help us know which area of SAST on which to focus:
- Tracking the # of SAST configurations (default, out of date, customized)
- Tracking the # of SAST jobs (increase coverage across repos)
- Tracking the # of issues identified by SAST
- Tracking the # of issues resolved that were identified by SAST
- Diversity of coverage of SAST jobs (language, type of identified issues, severity of issues)

### Roadmap
[The SAST Category Maturity level](https://about.gitlab.com/direction/maturity/#secure) is currently at `Viable`. We plan to mature it to `Complete` by January 2021. 

 - [SAST Vision](https://gitlab.com/groups/gitlab-org/-/epics/527)

## What's Next & Why
For the next few releases we are currently focused on cleaning up the state of our current scanners and improving support for additional configurations. We also want to bring our SAST scanners down to Core to make them available to the most people as part of our [community stewardship commitment](https://about.gitlab.com/company/stewardship/#promises).

**Immediate Priorities**
- [SAST Support for Air-gapped networks](https://gitlab.com/groups/gitlab-org/-/epics/2338)
- [.Net Framework Support](https://gitlab.com/gitlab-org/gitlab/issues/6289#note_276641786)
- [SAST to Core](https://gitlab.com/groups/gitlab-org/-/epics/2098)

**Why is this important?**

GitLab needs at least a minimum level of coverage in the SAST feature set to check the box for compliance and buyer personas. But further SAST has a very real impact to help the world write better code. If Gitlab provides a basic level of SAST to all repositories on Gitlab, we can meaningfully help protect against the simplest of code security issues. That encourages Gitlab to be the source of security information for repositories. It also provides opportunities to show the breadth of GitLab's feature set, and how that enables more complete and holistic DevOps processes.

**Differentiation**

Gitlab uniquely has opportunities within the entire devops process. We can integrate across different devops stages leveraging data, insight, and functionality from other steps to enrich and automate based on SAST findings.
We might in fact allow integration with competitors to ensure Gitlab controls the devops process, regardless of the specific SAST solution a team chooses, or that fits their unique needs. This centers GitLab as the system of control and allows people to extend and integrate other solutions into the GitLab Devops workflow.

## Maturity Plan

- [SAST to Complete](https://gitlab.com/groups/gitlab-org/-/epics/2895)

## Competitive Landscape
There are many well-known commercial products that are providing SAST. Most of them support multiple languages, and are integrated into the development lifecycle.

* Competitors are focused on a few areas:
    * Accuracy, breadth, scope of identifiable issues
    * More code language support
    * Detection Intelligence (AI/MI)
    * Solution automation

Here are some vendors providing SAST tools:
* [Checkmarx](https://www.checkmarx.com/products/static-application-security-testing)
* [Synopsis](https://www.synopsys.com/software-integrity/security-testing/static-analysis-sast.html)
* [SonarQube](https://www.sonarqube.org)
* [CA Veracode](https://www.veracode.com/products/binary-static-analysis-sast)
* [Fortify](https://software.microfocus.com/en-us/products/static-code-analysis-sast/overview)
* [IBM AppScan](https://www.ibm.com/security/application-security/appscan)

GitLab has the unique position to deeply integrate into the development lifecycle, with the ability to leverage CI/CD pipelines to perform the security tests. There is no need to connect the remote source code repository, or to use a different interface.

We can improve the experience even more, by supporting additional features that are currently present in other tools.

* [Support incremental scans for SAST](https://gitlab.com/gitlab-org/gitlab-ee/issues/9815)
* [Auto Remediation support for SAST](https://gitlab.com/gitlab-org/gitlab-ee/issues/9480)

## Analyst Landscape
We want to engage analysts to make them aware of the security features already available in GitLab. They also perform analysis of vendors in the space and have an eye on the future. We will blend analyst insights with what we hear from our customers, prospects, and the larger market as a whole to ensure we’re adapting as the landscape evolves. 

* [2019 Gartner Quadrant: Application Security Testing](https://www.gartner.com/en/documents/3906990/magic-quadrant-for-application-security-testing)
* [Gartner Application Security Testing Reviews](https://www.gartner.com/reviews/market/application-security-testing)
* [2019 Forester State of Application Security](https://www.forrester.com/report/The+State+Of+Application+Security+2019/-/E-RES145135)
* [OWASP SAST Tools](https://www.owasp.org/index.php/Source_Code_Analysis_Tools)
* [2019 StackOverflow Developer Survey](https://insights.stackoverflow.com/survey/2019#technology)

## Top Customer Success/Sales Issue(s)
[Full list of SAST issues](https://gitlab.com/gitlab-org/gitlab/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ASAST)

## Top user issue(s)
[Full list of SAST issues with Customer Label](https://gitlab.com/gitlab-org/gitlab/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ASAST&label_name[]=customer)

## Top internal customer issue(s)
- https://gitlab.com/gitlab-org/gitlab-ee/issues/9324

[Full list of SAST internal issues](https://gitlab.com/gitlab-org/gitlab/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ASAST&label_name[]=internal%20customer)

## Top Vision Item(s)
* [SAST Product Vision](https://gitlab.com/gitlab-org/gitlab/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ASAST&label_name[]=Product%20Vision%20FY21)

## Related Categories
* [DAST](https://about.gitlab.com/direction/secure/dynamic-analysis/dast/) - Dynamic Application Security Testing
* [Dependency Scanning](https://about.gitlab.com/direction/secure/composition-analysis/dependency-scanning/) - Evaluating Dependency Vulnerabilities 
* [Threat Insights](https://about.gitlab.com/direction/defend/vulnerability_management/) - Security Dashboard, Reports, and interacting with Vulnerabilities
