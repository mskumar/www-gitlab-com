---
layout: markdown_page
title: "Product Direction - Monitor"
---

## On this page
{:.no_toc}

- TOC
{:toc}

This is the product direction for Monitor. If you'd like to discuss this direction
directly with the product managers for Monitor, feel free to reach out to Dov Hershkovitch ([GitLab](https://gitlab.com/dhershkovitch), [Email](mailto:dhershkovitch@gitlab.com)), Sarah Waldner ([GitLab](https://gitlab.com/sarahwaldner), [Email](mailto:swaldner@gitlab.com) [Zoom call](https://calendly.com/swaldner-gitlab/30min)) or Kevin Chu ([GitLab](https://gitlab.com/kbychu), [Email](mailto:kchu@gitlab.com) [Zoom call](https://calendly.com/kchu-gitlab/30min)).

## Overview

The Monitor stage comes after you've configured your production infrastructure and
deployed your application to it. 

1. The Monitor stage is part of the verification and release process -
immediate performance validation helps to ensure your service(s) maintain
the expected service-level objectives ([SLO](https://en.wikipedia.org/wiki/Service-level_objective)s)
for your users.
1. The Monitor stage is an observability platform. [Observability](https://en.wikipedia.org/wiki/Observability) is the ability to infer internal states of a system based on the system’s external outputs.
Whether there are known ways to understand the total health of your systems, or your complex microservices system is full of unknowns, we want you to be able to export your system's telemetry to GitLab and use it to debug and diagnose any potential problem.
1. The Monitor stage helps you respond when things go wrong. It enables the aggregation of errors and alerts to identify problems and to find improvements.
The Monitor stage also enables responders to streamline incident response, so production issues are less frequent and severe.
1. The Monitor stage also provides is user feedback. Understanding how users experience your product and understanding how users actually use your product are critical to making the right improvements.

### Mission
The mission of the GitLab Monitor stage is to provide feedback that decreases the frequency and severity of incidents and improves operational and product performance.

## Landscape
The Monitor stage directly competes in several markets, including Application Performance Monitoring (APM), Log Management, Infrastructure Monitoring, IT Service Management (ITSM), Digital Experience Management (DEM) and Product Analytics. 
The [total addressable market for the Monitor stage was already more than $1.5 billion in 2018](https://docs.google.com/spreadsheets/d/1LO57cmXHDjE4QRf6NscRilpYA1mlXHeLFwM_tWlzcwI/edit?ts=5ddb7489#gid=2018046611) 
and is expected to grow as businesses continues to shift to digital.

All of these markets are well-established and crowded. 
However, they are also being disrupted by the underlying technologies used. The shift to cloud, containers, and microservices architectures changed users' expectation, and many existing vendors have struggled to keep pace. 
Successful vendors, such as market leader [Datadog](https://www.datadoghq.com/) have leveraged a platform strategy to expand their markets, and even stages within DevOps.

The changes in the market have also revealed opportunities that new entrants into this stage, like GitLab, can take advantage of.
Specfically, the [Ops section opportunities](https://about.gitlab.com/direction/ops/#opportunities) worth re-emphasizing are:
* *Clear winner in Kubernetes:* 
Driven by software-defined infrastructure, cost management, and resiliency, organizations are [flocking to cloud-native application architectures](https://drive.google.com/file/d/1ZAqTIiSfpHKyVFpgMnJoOBnyCQ-aF3ej/view).
Kubernetes is the clear winner in container orchestration platforms.
* *Continuing commoditization of tooling:* Well funded technology companies (Google, Netflix, Lyft, Spotify etc.), meeting scaling challenges, have a history of releasing open source software that leap-frog vendor software. 
Examples include Kubernetes, Prometheus, and many of the foundational projects in CNCF. DevOps vendors have to continue to move up the value chain as previously specialized software gets commoditized. 
We believe this will continue in the Monitor stage categories. A pertinent example is that vendor instrumentation is now considered a liability with OpenTelemetry(https://opentelemetry.io) (which allows organizations to become vendor agnostics) having received buy-in from most major APM vendors.

## Vision
In 2 year’s time, the Monitor stage categories of observability, incident management, and product feedback are the default choice for cloud-native teams using GitLab by being complete, cost effective, and simple to setup and operate, enabling continuous improvement.

GitLab is uniquely qualified to deliver on this bold and [ambitious](/handbook/product/#how-this-impacts-planning) vision because:

1. GitLab is a complete devops tool that is connected across the devops stages. Being one tool makes the circular devops workflow, and feedback, seamless and achievable.
2. The Monitor stage is pursuing a differentiated strategy from other observability vendors by not pursuing a usage based model business model by charging for processing and storage of observability.
Instead, we lean on powerful open source software, such as Prometheus and OpenTelemetry, along with commodity cloud services to enable customers to setup and operate Monitor stage observability solutions effectively. 
We will be successful because we are well-practiced in integrating different parts of the tool chain together.
3. Going cloud-native is a disruption to operations as usual. Cloud-native systems are constantly changing, are ephemeral, and are complex. 
As more and more companies adopt cloud-native, GitLab can create a well-integrated central control-pane that enables broad adoption by building on top of the tools that cloud-native teams are already familiar with and are using.

A trade-off in our approach is that we are explicitly not striving to be a fully turn-key experience that can be used to monitor all applications, particularly legacy applications.
Wholesale removing an existing monitoring solution is painful and a land and expand strategy is prudent here. As a customer recently explained, "Every greenfield application that we can deploy with your monitoring tools saves us money on New Relic licenses."
 
As this stage matures, we will begin to shift our attention and compete more directly with incumbent players as a holistic Monitoring solution for modern applications.

## 3 Year Strategy
Dovetailing on our 2 year vision statement, our 3 year goal is to have built an integrated package of observability and operations tools that can displace today's front-runner in modern observability, Datadog and compete in all Monitor categories. We'll do that by focusing on the four core workflows of Instrument, Triage, Resolve and Improve.

The following links describe our strategy for each individual workflow:

* [Instrument](https://gitlab.com/groups/gitlab-org/-/epics/1945) - Auto-detected and in app instrumentation, in code SLO definition and visual alert threshold setting
* [Triage](https://gitlab.com/groups/gitlab-org/-/epics/1947) - Starting with the highest level alert, using preconfigured dashboards to review relevant metrics, enabling ad-hoc visualization and immediate drill down from time sliced metrics into logs and traces in the same screen
* [Resolve](https://gitlab.com/groups/gitlab-org/-/epics/1972) - Access to automation and documentation for known remediations, integration of collaboration and user response tools
* [Improve](https://gitlab.com/groups/gitlab-org/-/epics/1973) - Automated incident review creation which compiles recorded information from the incident then track the code, infrastructure and observability improvements created by the incident. Track business, performance and availability metrics overtime.

## What's next

From 2020-05 through 2020-07, the following are the goals we are pursuing within the Monitor stage.

1. The Monitor::Health group is adding the [Alert Management](https://about.gitlab.com/direction/monitor/#alert-management) category so that customers can centralize all of the alerts from various systems within GitLab.
 * Key Result: [plan, execute, and collect feedback on GitLab's first game-days](https://gitlab.com/gitlab-org/monitor/health/-/issues/18)
1. The Monitor::APM group wants to enable every GitLab user to explore and triage issues for their Kubernetes based application starting from a GitLab metrics dashboard. 
 * Key Result 1: All public GitLab.com dashboards uses GitLab dashboards by end of July
 * Key Result 2: 20% increase in APM north star metric compared to the previous 3 month

The quarterly goals fit within the larger overarching objectives of the Monitor stage described below.

First, we plan to provide a streamline triage experience to allows our users to quickly identify and effectively troubleshoot an application problem as described in the following flow:

```mermaid
graph TB;
A[Alerts] -->|Embedded Metric Chart in Incident|B
B[Metrics] -->|Timespan Log Drilldown|C
C[Logs] -->|TraceID Search|D[Traces]
```
* Triage flow starts with triggered alert on breached metric
* Alert opens an incident from which the user can see the current status
* The user can drill directly from the incident into the relevant logs and search for the root cause analysis
* User can drill from each log into APM traces and view the stack trace

Detailed information can be found in the [triage to minimal epic](https://gitlab.com/groups/gitlab-org/-/epics/2225)

Second, we plan to [dogfood](#dogfooding) our current capabilities. Monitor and observability solutions,
by nature of what they are, have a high bar to meet before adoption. By continuing to improve the triage workflow, 
we will at the same time enable our GitLab teammates to use GitLap Monitor more fully. 

## Performance Indicators (PIs)

Our [Key Performance Indicator](https://about.gitlab.com/handbook/ceo/kpis/) for the Monitor stage is the **Monitor
SMAU** ([stage monthly active users](https://about.gitlab.com/handbook/product/metrics/#monthly-active-users-mau)).

Monitor SMAU is determined by tracking how users *configure*, *interact*, and *view* the features contained within the
stage. The following features are considered:

| Configure | Interact | View |
|-----------|----------|------|
|Install Prometheus|Add/Update/Delete Metric Chart|View Metrics Dashboard|
|Enable external Prometheus instance integration|Download CSV data from a Metric chart|View Kubernetes pod logs|
|Enable Jaeger for Tracing|Generate a link to a Metric chart|View Environments|
|Enable Sentry integration for Error Tracking|Add/removes an alert|View Tracing|
|Enable auto-creation of issues on alerts|Change the environment when looking at pod logs|View operations settings|
|Enable Generic Alert endpoint|Selects issue template for auto-creation|View Prometheus Integration page|
|Enable email notifications for auto-creation of issues|Use /zoom and /remove_zoom quick actions|View error list|
||Click on metrics dashboard links in issues||
||Click **View in Sentry** button in errors list||


See the corresponding [Periscope dashboard](https://app.periscopedata.com/app/gitlab/522840/Monitor-GitLab.com-SMAU) (internal).

<%= partial("direction/workflows", :locals => { :stageKey => "monitor" }) %>

<%= partial("direction/categories", :locals => { :stageKey => "monitor" }) %>

## Prioritization Process

We follow the same [prioritization guidelines](/handbook/product/product-management/process/#prioritization)
as the product team at large.

As noted above, in the short term the Monitor stage will be prioritizing ([video discussion](https://www.youtube.com/watch?v=nB5KDY4nsFg)) the following:
* Enabling and Dogfooding full DevOps cycle Auto DevOps including Metrics, Tracing, Logging, Alerts and Incidents
* [triage to minimal epic](https://gitlab.com/groups/gitlab-org/-/epics/2225)

You can see our entire public backlog for Monitor at this
[link](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Monitoring);
filtering by labels or milestones will allow you to explore. If you find
something you're interested in, you're encouraged to jump into the conversation
and participate. At GitLab, everyone can contribute!

Issues with the "direction" label have been flagged as being particularly
interesting, and are listed in the section below.

## Upcoming Releases

<%= direction["all"]["all"] %>

<%= partial("direction/other", :locals => { :stage => "monitor" }) %>
