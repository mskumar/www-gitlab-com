---
layout: markdown_page
title: "Category Direction - Continuous Delivery"
---
 
- TOC
{:toc}
 
## Continuous Delivery
 
Many teams are reporting that development velocity is stuck; they've
reached a plateau in moving to more and more releases per month, and now need help on how to improve. According to analyst research, 40% of software development
team's top priorities relate to speed/automation, so our overriding vision
for Continuous Delivery is to help these teams renew their ability to
accelerate delivery.
 
Additionally, Continuous Delivery serves as the "Gateway to Operations"
for GitLab, unlocking the downstream features such as the [Configure](/direction/ops/#configure)
and [Monitor](/direction/ops/#configure) stages.

- [Maturity Plan](#maturity-plan)
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AContinuous%20Delivery)
- [Overall Vision](/direction/ops/#release)
- [UX Research](https://gitlab.com/gitlab-org/ux-research/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aprogressive%20delivery&label_name[]=Category%3AContinuous%20Delivery)
- [Research insights](https://gitlab.com/gitlab-org/uxr_insights/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=Category%3AContinuous%20Delivery)
- [Documentation](https://docs.gitlab.com/ee/ci/)
 
### Continuous Delivery vs. Deployment
 
We follow the well-known definitions from Martin Fowler on the
difference between continuous delivery and continuous deployment:
 
- **Continuous Delivery** is a software development discipline
 where you build software in such a way that the software can
 be released to production at any time. You achieve continuous
 delivery by continuously integrating the software done by the
 development team, building executables, and running automated tests
 on those executables to detect problems. Furthermore you push the
 executables into increasingly production-like environments to
 ensure the software will work in production.
- **Continuous Deployment** means that every change goes through the
 pipeline and automatically gets put into production, resulting in many
 production deployments every day. Continuous Delivery just means that
 you are able to do frequent deployments but may choose not to do it,
 usually due to businesses preferring a slower rate of deployment. In order to do Continuous Deployment you must be doing Continuous Delivery.
 
_Source: [https://martinfowler.com/bliki/ContinuousDelivery.html](https://martinfowler.com/bliki/ContinuousDelivery.html)_
 
### Incremental Rollout
 
One of the key sub-areas within Continuous Delivery that we're tracking
is incremental rollout. This is important because it enables unprecedented
control over how you deliver your software in a safe way.  For these
items we're also tagging the label "incremental rollout", and you can
see an issue list of these items [here](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AIncremental%20Rollout).
Incremental rollout also serves as a key pillar for our [Progressive Delivery](/direction/ops/#progressive-delivery)
strategy.

### Infrastructure Provisioning
 
Infrastructure Provisioning and Infrastructure as Code, using solutions like Terraform or other provider-specific methods, is an interesting topic that relates to deployments but is not part of the Continuous Delivery category here at GitLab. For details on solutions GitLab provides in this space, take a look at the [category page](/direction/configure/infrastructure_as_code/) for our Infrastructure as Code team.
 
### Deployment with Auto DevOps
 
For deployment to Kubernetes clusters, GitLab has a focused category called Auto DevOps which is oriented around providing solutions for deploying to Kubernetes. Check out their [category page](/direction/configure/auto_devops/) for details on what they have planned.

We are working on a similar experience for non Kubernetes users, starting with [Streamline AWS Deployments](https://gitlab.com/groups/gitlab-org/-/epics/2351) that will automatically detect when users are deploying to AWS and will connect the dots for them.

The [Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/#auto-deploy) [jobs](https://gitlab.com/gitlab-org/gitlab/-/tree/master/lib%2Fgitlab%2Fci%2Ftemplates%2FJobs) within [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/) are maintained by the Continuous Delivery category.
 
## What's Next & Why

[Progressive Delivery](https://about.gitlab.com/direction/ops/#progressive-delivery) is a main pillar in Continuous Delivery, and one that we are investing lots of efforts in. 
We want to extend our support and discoverability of [Advanced deployments](https://gitlab.com/groups/gitlab-org/-/epics/2213), and are currently documenting how you can use Blue/Green deplyments with GitLab via [gitlab#14763](https://gitlab.com/gitlab-org/gitlab/-/issues/14763).

Another important theme that we are working on is [Do Powerful Things Easily](https://about.gitlab.com/direction/ops/#do-powerful-things-easily), which includes 
[Natively supporting hypercloud deployments](https://gitlab.com/groups/gitlab-org/-/epics/1804) and specifically deploying to [AWS](https://gitlab.com/groups/gitlab-org/-/epics/2351). Upcoming, we will be connect deployment to ECS to the autoDevOps flow via [gitlab#208132](https://gitlab.com/gitlab-org/gitlab/issues/208132), this will deploy automatically to ECS based on the AWS variables and AWS target that are defined in the project. 


## Maturity Plan
 
This category is currently at the "Complete" maturity level, and our next maturity target is Lovable (see our [definitions of maturity levels](/direction/maturity/)).
Key deliverables to achieve this are:
 
- [Limit pipeline concurrency using named semaphores](https://gitlab.com/gitlab-org/gitlab/issues/15536) (Complete)
- [Group deploy tokens](https://gitlab.com/gitlab-org/gitlab/issues/21765) (Complete)
- [Allow only forward deployments](https://gitlab.com/gitlab-org/gitlab/issues/25276)  (Complete)
- [https://gitlab.com/gitlab-org/gitlab/issues/14729](https://gitlab.com/gitlab-org/gitlab/issues/14729)
- [More out of the box incremental rollout options](https://gitlab.com/gitlab-org/gitlab-ee/issues/1387)
- [Streamline AWS Deployments](https://gitlab.com/groups/gitlab-org/-/epics/2351)
- [Allow fork pipelines to run in parent project](https://gitlab.com/gitlab-org/gitlab/issues/11934)
- [Post-deployment monitoring MVC](https://gitlab.com/gitlab-org/gitlab-ee/issues/8295)
- [Better guidance for more CD approaches](https://gitlab.com/gitlab-org/gitlab-ce/issues/52875)
- [Actionable CI/CD metrics](https://gitlab.com/gitlab-org/gitlab-ee/issues/7838)
 
## Competitive Landscape
 
Because CI and CD are closely related, the [competitive analysis for Continuous Integration](/direction/verify/continuous_integration#competitive-landscape)
is also relevant here. For how CD compares to other products in the market,
especially as it relates to pipelines themselves, also take a look there.
 
As far as CD specifically, Microsoft has always been strong in providing actionable metrics on
development, and as they move forward to integrate GitHub and Azure
DevOps they will be able to provide a wealth of new metrics to help
teams using their solution. We can stay in front of this by improving
our own actionable delivery metrics in the product via [gitlab-ee#7838](https://gitlab.com/gitlab-org/gitlab-ee/issues/7838).
 

### Harness

Harness is a modern, cloud-native CD platform
that provides excellent solutions for delivering to cloud environments
using native approaches like Kubernetes. Additionally, it manages the deployment all the way through
to monitoring, which we will introduce via [gitlab#8295](https://gitlab.com/gitlab-org/gitlab/issues/8295).
 We are planning to conduct research on [Harness](https://gitlab.com/gitlab-org/gitlab/-/issues/20307) and invite you to chime in on the issues and provide your insights and feedback as well.
 
### Spinnaker
 
Spinnaker is an open-source, multi-cloud continuous delivery platform that helps you release software changes.
It combines a powerful and flexible pipeline management system with integrations to the major cloud providers. 
Netflix is a big name attached, and the technology gets engineers excited. It takes time to integrate, but is not
a single app, so it can integrate with other things and you don’t have to replace your whole toolchain.

Overall Spinnaker is a great CD tool that treats deployments as a first class citizen by changing the mindset of its users. However,
it also has a weakness that it does not support other stages of the
DevOps lifecycle (such as Continuous Integration), while GitLab offers a single tool for your Development needs. 

Our goal is to do make it so anything that Spinnaker can
do, can also be done via built-in features in GitLab CD.

One analysis of GitLab vs. Spinnaker can be found on our [product comparison page](/devops-tools/spinnaker-vs-gitlab.html). There is another high quality one from our VP of product strategy in [gitlab#197709](https://gitlab.com/gitlab-org/gitlab/issues/197709#note_274765321). Finally, our PM for this section completed one where which can be found at [gitlab#35219](https://gitlab.com/gitlab-org/gitlab/issues/35219). Our next step is to review these and combine in this section of this document as the single source of truth, exhaustively listing the issues we plan to deliver to reach functional parity (or better) with Spinnaker.
 Spinnaker's advantage points are: 
 
* Provides easy application deployment across cloud providers. We have also recognised this as one of our top vision items and are working to [natively support hypercloud deployments](https://gitlab.com/groups/gitlab-org/-/epics/1804).
* Supports several deployment strategies which we are working on expanding in [Advanced Deploys](https://gitlab.com/groups/gitlab-org/-/epics/2213).
* Allows configuring an environment in standby for easy rollback, which is closely tied to [gitlab#35409](https://gitlab.com/gitlab-org/gitlab/issues/35409).
* Provides a user interface that allows you to view your infrastructure and to see exactly where your code is residing.  
* Acts as an operator that perform rollback when needed without the need to provide a script to do so (similar to Kubernetes) 

We also respect our customers choice to use Spinnaker's CD solution together with GitLab and are working on making
that integration easier with [gitlab#120085](https://gitlab.com/gitlab-org/gitlab/issues/120085).

If you are currently using Spinnaker as your CD solution, we would love to learn more. Please fill out our survey or register for
a customer interview at [gitlab#197709](https://gitlab.com/gitlab-org/gitlab/issues/197709).
 
## Analyst Landscape
 
In our conversations with industry analysts, there are a number of key trends
we're seeing happening in the CD space:
 
### Cloud Adoption
 
Cloud adoption of CI/CD is growing, with Docker adoption leading the
way and serverless likely next. People need guidance solving CD
because they are stepping out into the dark without mature solutions
already available to them; for example, AWS' current solution for
serverless (functions) CI is just "edit them live." Customers
complained that their products were starting to feel legacy, but
where they go next is unclear.
 
We invite you to follow our plans to [natively support hypercloud deployments](https://gitlab.com/groups/gitlab-org/-/epics/1804) and  [Serverless](https://about.gitlab.com/direction/configure/serverless/) to offer feedback or ask questions.
 
### Customer Experience
 
Customer experience is becoming a key metric. Users are looking for
the ability to not just measure platform stability and other
performance KPIs post-deployment, but also want to set targets for
customer behavior, experience, and financial impact, as part of [gitlab#37139](https://gitlab.com/gitlab-org/gitlab/issues/37139). Tracking and measuring these indicators after deployment solves an important
pain point. In a similar fashion, creating views which are managing
products not projects or repos will provide users with a more
relevant set of data.

### Progressive Delivery

Progressive Delivery is an important step in Continuous Delivery. We all want to ship faster and in smaller increments and progressive Delivery achieves exactly that with the addition of quick feedback. Having the ability to control the audience that gets the new feature set, gives developers ultimate control of exposure and if needed, rollback. It even allows QA to safely test in production.
 
### Other Topics

In addition to supporting the trends above, there are some key areas
where we can focus that will improve our solution in areas that
analysts are hearing customer demand:
 
- [gitlab#208132](https://gitlab.com/gitlab-org/gitlab/issues/208132): **Modeling (and autodiscovery/templatizing) of environments and deployments**
 as-code, and in a way that they can automatically interact with each
 other to generate deployment plans without requiring scripting. Views
 that make these complex relationships between environments and
 deployments clear.
- [gitlab#8295](https://gitlab.com/gitlab-org/gitlab/issues/8295): **Deployment status reporting, monitoring behavior, and error handling**,
 including [automated rollbacks](https://gitlab.com/gitlab-org/gitlab/issues/35404), [incremental rollouts](https://gitlab.com/gitlab-org/gitlab/issues/27264), and other intelligent
 built-in and customizable strategies and behaviors. This can include
 predictive analytics/ML and reporting about successes, failures, and pipeline health,
 both retrospective and runtime. Customizable and tailored for the
 user/role engaging with the data.

## Top Customer Success/Sales Issue(s)
 
The ability to monitor deployments and automatically halt/rollback deployment in case of exceeding a specific error rate is frequently mentioned by CS
and in the sales cycle as a feature teams are looking for. This will be
implemented via [gitlab#8295](https://gitlab.com/gitlab-org/gitlab/issues/8295).
 
## Top Customer Issue(s)
 
Our most popular customer request is [gitlab#30769](https://gitlab.com/gitlab-org/gitlab/-/issues/30769), 
which allows Deploy Keys with write access to push commits to protected branches.

## Top Internal Customer Issue(s)
 
Allowing forked pipelines to run in a parent project via [gitlab#11934](https://gitlab.com/gitlab-org/gitlab/issues/11934) is our most popular intrnal customer issue. It will especially help us verify that community contributions live peacefully with our tests before merging them.

### Delivery Team

Our delivery team is responsible for deploying to GitLab.com, which is the largest installation of GitLab on the planet, read more to check out our [CI/CD Blueprint](/handbook/engineering/infrastructure/library/ci-cd/) and learn about how we dogfood CI/CD at GitLab.

## Top Vision Item(s)
 
Our top vision item is to [Natively support hypercloud deployments](https://gitlab.com/groups/gitlab-org/-/epics/1804), we want to help make it easier and quicker to get started and deploy to any one of the big cloud providers using GitLab's CI/CD.
