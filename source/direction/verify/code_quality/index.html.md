---
layout: markdown_page
title: "Category Direction - Code Quality"
---

- TOC
{:toc}

## Code Quality

Automatically analyze your static source code to surface issues and see if quality is improving or getting worse with the latest commit. Our vision for Code Quality is to provide actionable data across an organization to empower users to make quality visible with every commit and in every release.

- [Maturity Plan](https://gitlab.com/groups/gitlab-org/-/epics/1302)
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ACode%20Quality)
- [Overall Vision](/direction/ops/#verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

Interested in joining the conversation for this category? Please join us in the issues where we discuss this topic and can answer any questions you may have. Your contributions are more than welcome.

This page is maintained by the Product Manager for Testing, James Heimbuck ([E-mail](mailto:jheimbuck@gitlab.com))

## What's Next & Why

Moving the [Code Quality Merge Request Widget to Core](https://gitlab.com/gitlab-org/gitlab/-/issues/212499) is the next item for the Code Quality category. As we think about Code Quality and displaying how a Merge Request is changing that value it was clear that this feature will be championed by the individual developer. This feature belongs in the GitLab Tier targeted at that user, Core.

Making the [full code quality report](https://gitlab.com/gitlab-org/gitlab/issues/4189) available was just a step in solving the problem teams have when they are not able to easily view a full code quality report. We know that the current version of this feature will be more powerful if users can find data about specific files and errors which is why we are working on adding [sort, filter and search](https://gitlab.com/gitlab-org/gitlab/-/issues/209795) to the code quality report next.

## Maturity Plan

This category is currently at the "Minimal" maturity level, and
our next maturity target is Viable (see our [definitions of maturity levels](/direction/maturity/)).
Key deliverables to achieve this are:

- ~~[CI view for code quality](https://gitlab.com/gitlab-org/gitlab/issues/4189)~~ - Released in 12.9
- [Code quality on `master`](https://gitlab.com/gitlab-org/gitlab/issues/2766)
- [Code quality notices on diffs/MRs](https://gitlab.com/gitlab-org/gitlab/issues/2526)

We may find in research that only some of these issues are needed to move the vision for this category forward. The work to move the vision is captured and being tracked in this [epic](https://gitlab.com/groups/gitlab-org/-/epics/1302).

## Competitive Landscape

### Azure DevOps

Azure DevOps does not offer in-product quality testing in the same way we do with CodeClimate, but does have a number of easy to find and install plugins in their [marketplace](https://marketplace.visualstudio.com/search?term=code%20quality&target=AzureDevOps&category=All%20categories&sortBy=Relevance) that are both paid and free. Their [SonarQube plugin](https://marketplace.visualstudio.com/items?itemName=SonarSource.sonarqube) appears to be the most popular, though it seems to have some challenges with the rating.

In order to remain ahead of Azure DevOps, we should continue to push forward the feature capability of our own open-source integration with CodeClimate. Issues like [gitlab-ee#2766](https://gitlab.com/gitlab-org/gitlab-ee/issues/2766) (per-branch view for how code quality progresses over time) moves both our vision forward as well as ensures we have a high quality integration in our product. To be successful here, though, we need to support formats Microsoft-stack developers use: [gitlab#29218](https://gitlab.com/gitlab-org/gitlab/issues/29218) gets the ball rolling. Because CodeClimate does not yet have deep .NET support, we may need to build something ourselves.

## Top Customer Success/Sales Issue(s)

An interesting suggestion from the CS team is [gitlab#8406](https://gitlab.com/gitlab-org/gitlab/issues/8406), which is beyond just code quality but code quality could lead the way. This item introduces instance wide code statistics, showing (for example) programming languages used, code quality statistics, and code coverage.

## Top Customer Issue(s)

The most popular feature issue is [gitlab#3985](https://gitlab.com/gitlab-org/gitlab/-/issues/3985) which will let C and C++ developers utilize the GitLab Code Quality features out of the box without having to customize their .gitlab-ci.yml files to get static analysis results.

Another top customer priority is to resolve the performance issues in parsing large code quality items in [gitlab#2737](https://gitlab.com/gitlab-org/gitlab/-/issues/2737)

## Top Internal Customer Issue(s)

Sharing line by line code quality data from a report ([gitlab#2526](https://gitlab.com/gitlab-org/gitlab/issues/2526)) is a useful feature for internal users that will help us make better decisions about if a merge request should be merged, or if it needs further refinement.

## Top Vision Item(s)

In terms of moving the vision forward, [gitlab#2766](https://gitlab.com/gitlab-org/gitlab/issues/2766) (which creates a per-branch view for code quality) is an interesting approach. This brings code quality tracking out of the MR widget, and looks at how code quality is changing over a longer period of time in the context of a branch (or master.)

We also want to provide better support for .NET users of GitLab. [gitlab#29218](https://gitlab.com/gitlab-org/gitlab/issues/29218) adds support for more traditional Microsoft stack code coverage tools, making GitLab just as competent as Azure DevOps at showing code coverage.
