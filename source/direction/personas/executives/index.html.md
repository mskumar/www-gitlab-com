---
layout: markdown_page
title: "Personas Vision - Executives"
---

- TOC
{:toc}

## Who are executives?
TBD

## What's next & why
- [Value stream management](/direction/manage/value_stream_management/)

### Walkthrough

This is an example of an executive flow we're trying to achieve with value
stream management. Note: these are mockups of how we see the future, but the
actual features might look very different or may not ship at all.

![VSM Flow](executiveflow.gif)

## Key Themes
- [Value stream management](https://gitlab.com/groups/gitlab-org/-/epics/668)
- [Project dashboard](https://gitlab.com/groups/gitlab-org/-/epics/436)
- [Code analytics](https://gitlab.com/groups/gitlab-org/-/epics/717)
- [Audit management](https://gitlab.com/groups/gitlab-org/-/epics/642)

## Stages with executive focus

There are several stages involved in developing the executive's toolbox at GitLab. These include, but are not necessarily limited to the following:
- [Plan](/direction/plan)
