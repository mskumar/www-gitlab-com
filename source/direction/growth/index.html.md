---
layout: markdown_page
title: Product Direction - Growth
description:
---

## Growth Section Overview

The Growth section at GitLab was formed as of August 2019 and we are iterating along the way. We are excited by the unique opportunity to apply proven growth approaches and frameworks to a highly technical and popular open core product. Our goal is to accelerate and maximize GitLab user and revenue growth, and while doing that, to become a leading B2B product growth team and share our learnings internally and externally.

Currently, our Growth section consists of 6 groups (acquisition, conversion, retention, expansion, fulfillment, telemetry), each consisting of a cross-functional team of Growth Product Managers, Developers, UX/Designers, with shared analytics, QA and user research functions.

### Growth Section's Principles

In essence, we are a cross-functional team of product managers, engineers, designers and data analysts with a unique focus and approach to help GitLab grow. Since GitLab Growth Section is still relatively new, we'd like to share the principles we strive to operate under, which will act as our team vision, and also help the rest of the company understand the best ways to collaborate with us.   

#### Principle 1: The Growth section focuses on connecting our users with our product value

While most traditional product teams focus on creating value for users via shipping useful product features, the Growth section focuses on connecting users with that value.  We do so by: 
* Driving feature adoption by removing barriers and providing guidance
* Lowering Customer Acquisition Cost (CAC) by maximizing the conversion rate across the user journey
* Increasing Life time value (LTV) by increasing retention & expansion
* Lowering sales/support cost by using product automation to do the work

#### Principle 2: The Growth section sits in the intersection of Sales, Marketing, Customer success & Product

Growth teams are by nature cross-functional as they interact and collaborate closely with many other functional teams, such as Product, Sales, Marketing, Customer success etc. Much of growth team's work helps to complement and maximize these team's work, and ultimately allow customers to get value from our product. 

![growth team helps](https://gitlab.com/gitlab-com/www-gitlab-com/-/raw/ece3ead382dbd91201f8e09246ecdc5c6c415643/source/images/growth/growthhelps.png)


To provide some examples:
* While the Marketing Team works on creating awareness and generating demand via channels such as webinars, content marketing & paid ads, the Growth team can help lower friction in signup and trial flows to maximize the conversion rate, thus generating higher ROI on marketing spend
* While the Sales Team works on converting larger customers in a high touch manner,the Growth team can help create self-serve flows and funnels to convert SMB customers, as well as building automated tools to make the sales process more efficient
* While the Core product Team works on building features to solve customer problems and pain points, the Growth team can help drive product engagement, create usage habit and promote multi-feature adoption
* While the Customer Success & Support Teams work on solving customers problems and driving product adoption via human interaction, the Growth Team can help build new user onboarding flows and on-context support to serve some of these needs programmatically at large scale

#### Principle 3: The Growth Section uses a data-driven and systematic approach to drive growth

Growth teams use a highly data & experiment driven approach
* We start from clearly defining what success is and how to measure it, by picking a [North Star Metric](https://about.gitlab.com/handbook/product/metrics/#north-star-metric) - the one metric that matters most to the entire business now
* Then we break down the NSM into sub-metrics, this process is called building a growth model. Growth model helps us identify key variables that contribute to GitLab's north star metric, and describes the connections between them at a high level. 
  We continuously iterate our thinking about GitLab's growth model as we learn more about our business, customers, and product, but our latest thinking is captured here [GitLab Growth Model](https://docs.google.com/presentation/d/1aGj2xCh6VhkR1DU07Nm3oaSzqDLKf2i64vlJKjn05RM/edit?usp=sharing). 


```mermaid

graph LR
	A[ARR] --> B[ARR from New Paying Customers]
	A --> C[ARR from Existing Paying Customers]
	B --> D[New customers directly sign up for Paid Plan]
	D --> E[# of Visitors]
	D --> F[Conversion Rate]
	D --> G[Average Contract Value]
	B --> H[New Trial Customers sign up for Paid Plan ARR]
	H --> I[# of Trials]
	H --> J[Conversion Rate]
	H --> K[Average Contract Value]
	B --> L[New/Existing Free Users sign up for Paid Plan ARR]
	L --> M[# of Free User Base]
	L --> N[Conversion Rate]
	L --> O[Average Contract Value]
	C --> P[Existing Paid Customer ARR]
	C --> R[Existing Customer Churned ARR]
	C --> Q[Existing Customer Expansion ARR]
	P --> S[Renewal]
	Q --> T[Seats]
	Q --> U[Upgrades]
	T --> V[# of Seats]
	T --> W[$ per Seat]
	U --> X[# of Upgrades]
	U --> Y[Delta ARR $]
	R --> Z[Cancel]
	R --> AA[Downgrade]
	AA --> AB[# of Downgrades]
	AA --> AC[Delta ARR $]
	subgraph Retention
	S
	Z
	end
	subgraph Expansion
	T
	U
	V
	W
	X
	Y
	AA
	AB
	AC
	end
	subgraph Acquisition
	E
	F
	G
	end
	subgraph Conversion
	I
	J
	K
	M
	N
	O
	end

```

Note that the terms used in growth model are broadly defined, for example, "Conversion Rate" here refers to the percentage of various non-paid customers (new, trial, free) converting to paid customers.

* Then by reviewing the sub-metrics, we try to identify the areas with highest potential for improvement, and pick focus area KPIs for those areas. We typically do so by looking for signals such as a very low conversion rate in an important funnel, metrics that seem to be low comparing with industry benchmarks, or a product flow that caused a lot of customer complaints or support tickets etc.         
* Then we use the build-measure-learn framework to test different ideas that can potentially improve the focus area  KPI. We utilize an experiment template to capture the hypothesis all the way to the experiment design, rank all experiment ideas using ICE framework, work through the experiment backlog, and then analyze the experiment results.
* Once this focus area KPI has been improved, we go back to the growth model, and identify the next areas we want to focus on 

```mermaid
graph LR
    nsm[North Star Metric]--> grm
    grm[Growth Model] --> fak
    fak[Focus Area KPI] -- Data --> ide
    ide[Ideate] --> pri
    pri[Prioritize] --> bld
    bld[Build] --> alz
    alz[Analyze] --> tst
    tst[Test] --> ide
    
    style nsm fill:#6b4fbb
    style nsm color:#ffffff
    style grm fill:#6b4fbb
    style grm color:#ffffff
    style fak fill:#fba338
    style fak color:#ffffff
    style ide fill:#fa7035
    style ide color:#ffffff
    style pri fill:#fa7035
    style pri color:#ffffff
    style bld fill:#fa7035
    style bld color:#ffffff
    style alz fill:#fa7035
    style alz color:#ffffff
    style tst fill:#fa7035
    style tst color:#ffffff
```

By following this systematic process, we try to make sure that: 1) We know what matters most; 2) Teams can work independently but their efforts will all contribute to overall growth ; 3) We always aim to work on the projects with the highest ROI at any given moment. The ultimate goal is that we are not only uncoverying all levers to drive growth, but also tackling them as efficienly as possible. 
#### Principle 4: The Growth Section experiments a lot and has a “Win or Learn”  mindset

***"Experiments do not fail, hypotheses are proven wrong"***

Growth teams view any changes as an “experiment”, and we try our best to measure the impact. With well-structured hypotheses and tests, we can obtain valuable insights that will guide us towards the right direction, even if some of the experiments are not successful in term of driving desired metrics. 

Below is a sample list of our currently running and concluded experiments. 

| Experiment Name | Group | Status |
| ------ | ------ |------ |
|[Update .com paid sign up flow](https://gitlab.com/gitlab-org/growth/product/issues/87)|Acquistion|Live|
|[Pricing page tier presentation test](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/73)|Acquistion|Iterating|
|[Display security navigation to upsell new .com signups](https://gitlab.com/gitlab-org/gitlab/-/issues/34910)|Conversion|Live|
|[.com Trial flow improvments](https://gitlab.com/gitlab-org/gitlab/-/issues/119015)|Conversion|[Concluded](https://gitlab.com/gitlab-org/gitlab/-/issues/215881)|
|[MR with no pipepline Prompt](https://gitlab.com/gitlab-org/growth/product/issues/176)|Expansion|Live|
|[Clarify Auto renew toggle vs. Cancel](https://gitlab.com/gitlab-org/growth/product/-/issues/43) |Retention |[Concluded](https://gitlab.com/gitlab-org/growth/product/issues/162)|



## Growth Section's Current Focus

### FY21 Q2 Growth Section OKR

Please refer to [here](https://about.gitlab.com/company/okrs/fy21-q2/) for latest GitLab Company & Growth Section OKR


## Growth Section's FY21 Plan

In 2020, Covid-19 has brought uncertainty and disruptions to the market. In tough economic times, all businesses need to focus on efficient growth. GitLab's Growth Section aims to help the company drive growth while improving efficiency in all fronts. Therefore we've aligned our 1-Year Plan into the 3 themes below: 

#### Theme 1: Improve customer retention to build a solid foundation for growth

Existing customer retention is our life blood, especially in tough marco-economic environments, it is critical to serve our current customers well and minimize churn. GitLab has a unique advantage with a subscription business model, and our[ gross retention](https://about.gitlab.com/handbook/product/performance-indicators/#gross-retention) is fairly strong, however if you drill down to look at [cohort-based subscription retention](https://docs.google.com/presentation/d/1RF8b0TYIgnPy1bQpwNvLYT_AIIvu2po9V4yWv9pXVAo/edit#slide=id.g83c44e3899_5_0), we still have room for improvement. On the other hand, in tough times, our customers will potentially be facing challenge and uncertainty as well, therefore it will be naturally harder to upsell them with higher tiers or more seats. By staying laser focused on retention, we can build a strong foundation to weather the storm and drive growth sustainability, and can be right there when customers are ready to expand again. 

To improve retention, the Growth Section has identified and will be working on the areas such as: 


1) Continue to make the customer purchasing flows such as renewal and billing process robust, efficient, transparent and smooth, as these are critical customer moments.  
We have done projects such as: 
* Clarifying the options between “auto-renewal” and “cancel” 
* [Providing clear information on customer licenses and usage](https://gitlab.com/gitlab-org/gitlab/-/issues/118592) 
* Automating renewal processes to minimize sales team burden

And we are also working on quarterly co-term billing for self-hosted customers, continious improvement of customer portal user experience, as well as supporting critical pricing and packaging related projects. 

To make sure what we do will benefit customer, we have a weekly cross-functional sync with sales, biz ops, support, finance and closely monitor Customer Success Score (billing related) as our OKR. 

2)  Deep dive into the customer behavior and identify leading indicators for churn. This will include projects such as:  
* Creating [Customer MVC health score](https://gitlab.com/gitlab-data/analytics/-/issues/3803) and analyze usage based retention curve with help from data analytics team
* Capturing and analyzing [reasons for cancellation](https://app.periscopedata.com/app/gitlab/572742/Auto-Renew-AB-Test-Result?widget=7523649&udv=0) 
* Experimenting with different types of “early interventions” to reduce churn

3) Identify and experiment on drivers that can lead to better retention. 

Through analysis, we can identify which customer behaviors are critical to retention, and therefore we can experiment on prompting these types of behaviors among more customers. For example,  one hypothesis is that if a customer uses multiple stages and features of GitLab, they are more likely to get value from a single DevOp platform, thus they are more likely to become a long term customer. 

Therefore, in FY21, one of the Growth Section's focus areas will be on helping GitLab customers adopt more stages, and we'll observe if that leads to better retention to confirm the causation. ALong with this, we will need to conduct both [quantitative data analysis](https://gitlab.com/gitlab-data/analytics/-/issues/4762) and [quanlitative research](https://gitlab.com/gitlab-org/growth/product/-/issues/1572) to describe the baseline and understand the drivers and barriers behind stage adoption.  

```mermaid
graph LR
    Create --> Plan
    Verify --> Secure
    Create --> Manage 
    Create --> Verify
    Verify --> Release
    Verify --> Package
    Release --> Monitor
    Release --> Configure
    Release --> Defend
```


##### Theme 2: Increase new customer conversion rate to maximize growth efficiency

In order to grow efficiently,  we also want to maximize the efficiency of new customer acquisition. For each marketing dollar we spend, we want to bring more dollars back in terms of revenue, and we also need to reduce payback time to generate cash flow that is essential for weathering storms.

As a collaborator to marketing & sales teams, the role the growth team can play here is to aggressively analyze, test and improve the new user flow and in-product experience between about.gitlab.com, GitLab.com, self-hosted instance, customer portal etc., with the goal of converting a prospect who doesn’t know much about GitLab, to a customer who understands the value of GitLab and happily signs up for a paid plan. 

In order to achieve this goal, we try to understand what the drivers are leading to new customer conversion and amplify them. For example, we have identified through analysis that the number of users in a team, as well the numbers of stages/features a team tries out, all seem to be correlated with a higher conversion rate to a paid plan.  Accordingly, we planed experiments and initiatives this area such as: 

1) Building a [new customer onboarding tutorial](https://gitlab.com/gitlab-org/growth/product/-/issues/107) to help users learn how to use different stages of GitLab 

2) Design better team invitation flow & triggers to increase the team size

3) Setting up [customers behavior triggers](https://gitlab.com/gitlab-org/gitlab/-/issues/215081) to enable the growth marketing team to send targeted & triggered emails 

Again, in order to drive this theme, we will also need to understand the end to end new customer journey, and identify the drivers and barriers via a series of research and analytics projects, such as: 


1) Map out GitLab new customers journey and understand any potential experience or data gaps between marketing, sales, growth, product teams 

2) Post-purchase survey & Email survey to understand the reasons behind why some customers convert, while others don’t 

3) Self-hosted new customer user research


##### Theme 3: Build out infrastructures and processes to unlock data-driven growth

Data is key to a growth team’s success. Growth team uses data & analytics daily to: define success metrics, build growth models, identify opportunities, and measure progress.  So part of our focus for FY21 will be working closely with GitLab data analytics team to build out that foundation, which include: 

1) Provide knowledge & [documentation](https://docs.google.com/presentation/d/1J0dctOP-xERo2j0WMWcaBuNqTNaaPuJTxLfIaOhNRHk/edit#slide=id.g74d58e3ba0_0_5) of currently available data sources to understand customer behaviors . Also, establish a framework and best practices to enable consistent and compliant data collection

2) Build out a Product usage metrics framework to evaluate how customers engage with certain features. For example, we successfully finished SMAU data collection and dashbaording project for all GitLab product stages in Q1, and will move on to a similar [North Star Metric project](https://gitlab.com/gitlab-org/telemetry/-/issues/374) for all GitLab product features in Q2. 

3) Build out Customer journey metrics framework to understand how customers flow the GitLab through funnel, including [end to end cross-functional reporting](https://gitlab.com/gitlab-data/analytics/-/issues/4336) spanning marketing, sales and growth.   


Along the way, we also aim to share our learnings and insights with the broader GitLab team and community, as we firmly believe growth is a whole company mission, and that success ultimately comes from constant learning & iteration & testing & sharing, as well as breaking the silos of functional teams to drive towards the same goal. 

Below is a sample of the dashboards we build and use daily. Many of these dashboards are used not only by us, but also by exectutives, broader product team, sales and customer success teams. Over time, the Growth Section wants to become an "insight hub" to constantly share learnings that can empower other GitLab teams.  

|Area |Dashboard/Report | Description |
| ------ | ------ |------ |
|Overall Product Usage| [SMAU dashboard](https://app.periscopedata.com/app/gitlab/634200/Usage-Ping-SMAU-Dashboard)  | Stage Monthly Active User trend for both .com and self managed |
|Overall Product Usage| [Stage Retention and Adoption Dashboard](https://app.periscopedata.com/app/gitlab/621735/WIP:-SMAU-Retention-and-Adoption) | How popular are the stages among GitLab customers, and how well are they retaining customers |
|Acquisition |[New Customer Acquisition Dashboard](https://app.periscopedata.com/app/gitlab/531526/Acquisition-Dashboard---Last-90-days) |Trend of acquistion of new customers |
|Conversion |[New Customer Conversion Dashboard](https://app.periscopedata.com/app/gitlab/608522/Conversion-Dashboard) | How are new users converting to customers and what are the drivers |
|Retention |[Renewal Dashboard](https://app.periscopedata.com/app/gitlab/505939/Renewals-Dashboard) | Trend on renewal and cancellation of susbscriptions|
|Retention |[Retention Dashboard](https://app.periscopedata.com/app/gitlab/403244/Retention) | Retention metrics and trend |
|Expansion | [Churn/Expansion by Segments Dashboard](https://app.periscopedata.com/app/gitlab/484507/Churn-%7C-Expansion-by-Sales-Segment) | Churn/Expansion reason and sales segment |
|Expansion | [SPAN Deep Dive Report](https://docs.google.com/document/d/1zt3uUcPJW7Y1dMgtIsi4Z1antbPFPy8fp7Fib4Ps_qw/edit?usp=sharing) |How to understand SPAN (Stage Per Average Namespace) and ideas to improve SPAN|

## Growth Section Areas of Focus

### 1. Apps & Services we focus on:
*   [GitLab Enterprise Edition (Self-Managed)](https://about.gitlab.com/handbook/engineering/projects/#gitlab)
*   [GitLab-com (SaaS)](https://gitlab.com/gitlab-com/www-gitlab-com#www-gitlab-com)
*   [Version.GitLab](https://about.gitlab.com/handbook/engineering/projects/#version-gitlab-com)
*   [Customers.GitLab](https://about.gitlab.com/handbook/engineering/projects/#customers-app)
*   [License.GitLab](https://about.gitlab.com/handbook/engineering/projects/#license-app)
*   [About.GitLab.com](https://about.gitlab.com/)

### 2. Growth Group Directions
* Acquisition Direction
* [Conversion Direction](https://about.gitlab.com/direction/conversion/)
* [Expansion Direction](https://about.gitlab.com/direction/expansion/)
* [Retention Direction](https://about.gitlab.com/direction/retention/)
* [Fulfillment Direction](https://about.gitlab.com/direction/fulfillment/)
* [Telemetry Direction](https://about.gitlab.com/direction/telemetry/)


### 3. What's next: Growth Group Issue Boards


* [Acquisition board](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aacquisition)
* [Conversion board](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion)
* [Expansion Board](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aexpansion) 
* [Retention Board](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aretention)
* [Fulfillment Board](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Afulfillment) 
* [Telemetry Board](https://gitlab.com/gitlab-org/telemetry/-/boards) 

### Helpful Links
*   [Growth Section](https://about.gitlab.com/handbook/engineering/development/growth/)
*   [Growth Product Handbook](https://about.gitlab.com/handbook/product/growth/)
*   [UX Scorecards for Growth](https://gitlab.com/groups/gitlab-org/-/epics/2015)
*   [GitLab Growth project](https://gitlab.com/gitlab-org/growth)
*   [KPIs & Performance Indicators](https://about.gitlab.com/handbook/product/metrics/)
