---
layout: markdown_page
title: "Diversity, Inclusion & Belonging  Events"
---

## On this page
{:.no_toc}

- TOC
{:toc}

##  Introduction

On this page you will be provided an overview of the Diversity, Inclusion & Belonging  events/journey that have taken place or will take place.


##  Summary of Events for 2019:

| Month    | Events                                                         |
|----------|----------------------------------------------------------------|
| Apr | Hired DIB Manager                                              |
| May | DIB Breakout Sessions at Contribute 2019                       | 
|          | [Inclusive Language Training](https://docs.google.com/presentation/d/186RK9QqOYxF8BmVS15AOKvwFpt4WglKKDR7cUCeDGkE/edit?usp=sharing)                                  |
|          | [Published GitLab DIB Mission Statement](/company/culture/inclusion/#diversity-inclusion--belonging-mission-at-gitlab)
|          | [Published GitLab's Definition of Diversity, Inclusion & Belonging ](/company/culture/inclusion/#gitlabs-definition-of-diversity-inclusion--belonging)
| Jul | Launched Greenhouse Inclusion Tool
| Aug | GitLab Pride launched                                          | 
|          | [GitLab MIT - Minorities in Tech](https://about.gitlab.com/company/culture/inclusion/erg-minorities-in-tech/) launched                       | 
|          | [GitLab DiversABILITY](https://about.gitlab.com/company/culture/inclusion/erg-gitlab-diversability/) launched                                  | 
|          | [GitLab Women](https://about.gitlab.com/company/culture/inclusion/erg-gitlab-women/) launched                                         | 
|          | [DIB Advisory Group launched](https://about.gitlab.com/company/culture/inclusion/advisory-group-members/)                                    | 
| Sep | [DIB Advisory Group Guidelines](https://about.gitlab.com/company/culture/inclusion/advisory-group-guide/) published
|          | [Published ERG Guidelines](https://about.gitlab.com/company/culture/inclusion/erg-guide/)                                       | 
|          | [DIB Framework](https://docs.google.com/presentation/d/1OMgmYc52J02PWacw72ZM_c-R6FYni-BibAhfV514KcQ/edit?usp=sharing)                                                  | 
| Oct | [Slack Channels for all ERGs and DIB Advisory Group added](https://about.gitlab.com/company/culture/inclusion/#ergs---employee-resource-groups)       | 
| Dec | [Live Learning Inclusion Training](https://www.youtube.com/watch?v=gsQ2OsmgqVM&feature=youtu.be)                              | 
|          | [Received DIB Comparably Award](https://about.gitlab.com/blog/2020/01/29/comparable-awards/?utm_medium=social&utm_source=linkedin&utm_campaign=blog)


#  Summary of Events for 2020:

| Month    | Events                                                          | 
|----------|-----------------------------------------------------------------|
| Jan | [Live Learning Ally Training](https://www.youtube.com/watch?v=wwZeFjDc4zE&feature=youtu.be)                                     | 
| Feb | Anita Borg becomes an Official Partner                          | 
|          | DIB Survey via Culture Amp                                      |                        |
| Mar | Unconscious Bias Training                                       | Being scheduled           |
|          | Working Mother Media Award Submission                           |                        |
|     | [Published Building an Inclusive Remote Culture Page](https://about.gitlab.com/company/culture/inclusion/building-diversity-and-inclusion/)           |                         |
|     | Published Diversity, Inclusion & Belonging  Events Page                     |                           |
| Apr      | Kickoff Women in Sales Initiatives                              |                        |
|          | Kickoff DIB in Engineering Initiatives                          |                        |
|          | Created the [Parental Leave Toolkit](https://about.gitlab.com/handbook/total-rewards/benefits/parental-leave-buddy/) for Managers and Team Members|
|          | DIB Sessions at Virtual Contribute 2020                         |                           |
