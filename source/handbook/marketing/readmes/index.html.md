---
layout: handbook-page-toc
title: "GitLab Marketing Team READMEs"
---

## Marketing Team READMEs

- [Darren M.'s README (Head of Remote)](/handbook/marketing/readmes/dmurph/)
- [William Chia's README (Sr. PMM)](/handbook/marketing/readmes/william-chia.html)
- [Parker Ennis README (Sr. PMM)](/handbook/marketing/readmes/parker-ennis.html)
