---
layout: markdown_page
title: "Usecase: Continuous Integration"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

#### Who to contact

| Product Marketing | Technical Marketing |
| ---- | --- |
| @parker_ennis  | @iganbaruch |

## Continuous Integration

The Continuous Integration (CI) use case is a staple of modern software development in the digital age. It's unlikely that you hear the word "DevOps" without a reference to "Continuous Integration and Continuous Delivery" (CI/CD) soon after. In the most basic sense, the CI part of the equation enables development teams to automate building and testing their code.

When practicing CI, teams collaborate on projects by using a shared repository to store, modify and track frequent changes to their codebase. Developers check in, or integrate, their code into the repository multiple times a day and rely on automated tests to run in the background. These automated tests verify the changes by checking for potential bugs and security vulnerabilities, as well as performance and code quality degradations. Running tests as early in the software development lifecycle as possible is advantageous in order to detect problems before they intensify.

CI makes software development easier, faster, and less risky for developers. By automating builds and tests, developers can make smaller changes and commit them with confidence. They get earlier feedback on their code in order to iterate and improve it quickly increasing the overall pace of innovation. Studies done by DevOps Research and Assessment (DORA) have shown that [robust DevOps practices lead to improved business outcomes](https://cloud.google.com/devops/state-of-devops/). All of these "DORA 4" metrics can be improved by using CI:
- **Lead time:** Early feedback and build/test automation help decrease the time it takes to go from code committed to code successfully running in production.
- **Deployment frequency:** Automated build and test is a pre-requisite to automated deploy.
- **Time to restore service:** Automated pipelines enable fixes to be deployed to production faster reducing Mean Time to Resolution (MTTR)
- **Change failure rate:** Early automated testing greatly reduced the number of defects that make their way out to production.

[GitLab CI/CD](/stages-devops-lifecycle/continuous-integration/) comes built-in to GitLab's complete DevOps platform delivered as a single application. There's no need to cobble together multiple tools and users get a seamless experience out-of-the-box.

## Personas

### User Persona

The typical **user personas** for this usecase are the Developer, Development team lead, and DevOps engineer.

#### Software Developer [Sacha](/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer)

Software developers have expertise in all sorts of development tools and programming languages, making them an extremely valuable resource in the DevOps space. They take advantage of SCM and CI capabilities together to work fast and consistently deliver high quality code.

- Developers are problem solvers, critical thinkers, and love to learn. They work best on planned tasks and want to spend a majority of their time writing code that ends up being delivered to customers in the form of lovable features.

#### Development Team Lead [Delaney](/handbook/marketing/product-marketing/roles-personas/#delaney-development-team-lead)

Development team leads care about their team's productivity and ability to deliver planned work on time. Leveraging CI helps maximize their team's productivity and minimize disruptions to planned tasks.

- Team Leads need to understand their team's capacity to assign upcoming tasks, as well as help resolve any blockers by assigning to right resources to assist.

#### DevOps Engineer [Devon](/handbook/marketing/product-marketing/roles-personas/#devon-devops-engineer)

DevOps Engineers have a deep understanding of their organization's SDLC and provide support for infrastructure, environments, and integrations. CI makes their life easier by providing a single place to run automated tests and verify code changes integrated back into the SCM by development teams.

- DevOps Engineers directly support the development teams and prefer to work proactively instead of reactively. They split their time between coding to implement features and bug fixes, and helping developers build, test, and release code.

### Buyer Personas

CI purchasing typically does not require executive involvement. It is usually acquired and installed via our freemium offering without procurement or IT's approval. This process is commonly known as shadow IT. When the upgrade is required, the [Application Development Manager](/handbook/marketing/product-marketing/roles-personas/buyer-persona/#alex---the-application-development-manager) is the most frequent decision-maker. The influence of the [Application Development Director](/handbook/marketing/product-marketing/roles-personas/buyer-persona/#dakota---the-application-development-director) is notable too.  

## Industry Analyst Resources

Examples of comparative research for this use case are listed just below. Additional research relevant to this use case can be found in the [Analyst Reports - Use Cases](https://docs.google.com/spreadsheets/d/1vXpniM08Ql0v0yDd22pcNmXpDrA-NInJOwj25PRuHXA/edit?usp=sharing) spreadsheet.

- [Forrester Wave for Cloud-Native CI Tools](/analysts/forrester-cloudci19/)
- [Forrester Continuous Integration Tools](/analysts/forrester-ci/)

## Market Requirements

| Market Requirement | Description | Typical capability-enabling features | Value/ROI |
|---------|-------------|-----------|------|
| 1) Build and test automation | Streamlines application development workflow by connecting simple, repeatable, automated tasks into a series of interdependent automatic builds and associated tests. Run and manage automated tasks in the background, preview and validate changes before it's merged to production. **Ensure software is consistently built and tested without manual intervention, enabling developers to get rapid feedback if their code changes introduce defects or vulnerabilities.** Teams have control over where automated jobs run either in the cloud (public/private/hybrid) or using shared infrastructure. | CI/CD pipelines, scalable resources, job orchestration/work distribution, automated deployments, caching, external repository integrations, and the ability to run isolated, automated, tests (such as unit tests, regression tests, etc.). | Development teams can work with speed and efficiency. Catch potential errors sooner rather than later before they intensify. |
| 2) Pipeline configuration management | The CI solution makes it easy for developers to automate the build and test workflow, specifically connecting to their source code repository, defining specific actions/tasks in build pipelines, and set parameters for exactly how/when to run jobs, using scripts and/or a GUI to configure pipeline changes. Configurations are easily reproducible and traceability exists to allow for quick comparisons and tracking of changes to environments. | Configurations via web UI or supports config-as-code in a human readable syntax, like YAML. | Maximize development time and improves productivity. Less manual work. |
| 3) Visibility and collaboration | The solution enables development teams to have visibility into pipeline structure, specific job execution, and manage changes and updates to the pipeline jobs. The solution should enable teams to easily collaborate and make code changes, review work, and communicate directly. CI solutions should provide visibility and insights into the state of any build. | Pull requests or merge requests, commit history, automatic notifications/alerts, chatOps, code reviews, preview environments. | Faster feedback and less risk that changes cause builds to break. |
| 4) Platform and language support | Many organizations use a mix of operating systems and code stacks within their development teams.  The CI solution must operate on multiple operating systems (Windows, Linux, and Mac). The CI solution must also be language-agnostic supporting automation of build and test for a wide variety of development languages(Java, PHP, Ruby, C, etc.). | Option to use native packages, installers, downloads from source, and containers. Cloud-native platform support for public, private, or hybrid hosting. Runs on any OS. | Gives teams more flexibility, making it easier to adopt. |
| 5) Pipeline security |  Building security into an automated process ensures access to CI pipelines is consistently managed and controlled. Facilitate secure access to jobs/branches for the right users, safely store/manage sensitive data (such as secrets), and enforce governance and compliance policies without slowing anyone down. | Access control (like RBAC), enterprise-based access systems (like LDAP), data encryption, secrets management, and secure network connections. | Reduces business risk and protects intellectual property. Instills confidence in end-users. |
| 6) Built in compliance | The solution has capabilities and policies built in to ensure that code being delivered is compliant and secure in the event of an audit. Be able to track and manage important events, trace them back to who performed specific actions, as well as when they occurred.  | Automated security and compliance testing baked into CI pipelines. Automatic notifications for compliance issues. Audit controls and policies in place.  | Reduce risk and discover flaws or potential violations earlier in the development process.  |
| 7) Easy to get started | The steps required to successfully setup CI should be simple and straightforward with minimal barrier to entry. The time and effort required for teams to get started from initial installation,  configuration, onboarding users, and delivering value should be short (days, not weeks). | Supports both on-premise and SaaS implementations. Has robust documentation outlining steps to connect to a repository and get a CI server up and running quickly. Supports configuration-as-code or provides a GUI for initial configurations. Web interface to provision users. | Faster time to value. |
| 8) DevOps tools and integrations | The solution supports strong integrations to both upstream and downstream processes such as project management, source code management and version control, artifact repositories, security scanning, compliance management, continuous delivery, etc. A CI solution with strong DevOps integrations means there's flexibility for users who need or want a balance between native capabilities and integrations. | Integrations with build automation tools (Gradle, Maven etc.), code analysis tools, binary repos, CD tools, IDEs, APIs, third party libraries or extensibility via plugins. | Increases efficiency. Lessens cost and the extra work that comes along with potential migrations. |
| 9) Analytics | Capture and report on pipeline performance metrics and data throughout the build/test processes in order to identify bottlenecks, constraints, and other opportunities for improvement. Identify failing tests, code quality issues, and valuable trends in data indicative of overall application quality. | Build health, pipeline visualization, code coverage, logging, and performance metrics such as deployment frequency. | Increase efficiencies and reduce unnecessary costs. |
| 10) Elastic scalability | The solution supports elastic scaling leveraging well understood, third-party, mechanisms whenever possible. Supports build scalability around existing native per-cloud and per-virtual environment vendor capabilities since they represent already debugged code for a complex problem, and the customers architects/operators are much more likely to be familiar with these mechanisms in their chosen infrastructure/cloud provider. | Supports ephemeral provisioning. Utilizes strong Kubernetes integration at-scale or supports non-Kubernetes scaling options. For example, use AWS Autoscaling Groups in AWS, GCP scaling in GCP, and Azure Autoscale in Azure. | Faster and more efficient. Reduce infrastructure overhead with on-demand resources. Provides flexibility to scale large workloads using popular cloud providers. |
| 11) Artifact and dependency management | The solution allows for easy management of outputs and binaries from the build/test process. Manage repos, application packages, and containers with visibility into their dependencies. | View and download artifacts. Modify, store, and share images. Support for a wide range of common package formats and third-party integrations. Can be used on-prem or in the cloud. | Streamlines your CI/CD workflow and speeds up development. |


## How GitLab Meets the Market Requirements

A collection of short demonstrations that show GitLab's CI capabilities.

* [Build and test automation](https://youtu.be/rti7T1yGrlw)
* [Pipeline configuration management](https://youtu.be/opdLqwz6tcE)
* [Visibility and collaboration](https://youtu.be/z8r3rFQT8xg)
* Platform and language support
* Pipeline security
* Built in compliance
* [Easy to get started](https://youtu.be/e0iQD1qgxZg)
* DevOps tools and integrations
* Analytics
* Elastic scalability
* Artifact and dependency management


## GitLab Stages and Categories

At GitLab, we address these market requirements through features included in these stages/categories:

[**Manage**](/stages-devops-lifecycle/manage/)
* Audit Events, Compliance Management

[**Verify**](/stages-devops-lifecycle/verify/)
* CI, Code Quality, Code Testing and Coverage, Web Performance, Usability Testing, Accessibility Testing, Merge Trains

[**Package**](/stages-devops-lifecycle/package/)
* Package Registry, Container Registry, Dependency Proxy

[**Release**](/stages-devops-lifecycle/release/)
* Review Apps


## Top GitLab Features for CI

* **Multi-platform:** you can execute builds on Unix, Windows, OSX, and any other platform that supports Go.
* **Multi-language:** build scripts are command line driven and work with Java, PHP, Ruby, C, and any other language.
* **Parallel builds:** GitLab splits builds over multiple machines for fast execution.
* **Autoscaling:** you can automatically spin up and down VM's or Kubernetes pods to make sure your builds get processed immediately while minimizing costs.
* **Realtime logging:** a link in the merge request takes you to the current build log that updates dynamically.
* **Versioned tests:** a .gitlab-ci.yml file that contains your tests, allowing developers to contribute changes and ensuring every branch gets the tests it needs.
* **Flexible Pipelines:** define multiple jobs per stage and even trigger other pipelines.
* **Build artifacts:** upload binaries and other build artifacts to GitLab. Easily browse and download them.
* **Test locally:** reproduce tests locally using `gitlab-runner exec`.
* **Docker support:** use custom Docker images, spin up services as part of testing, build new Docker images, run on Kubernetes.
* **Container Registry:** built-in container registry to store, share, and use container images.
* *we'll add to this list as we continue to build out more lovable features!*

## Top 3 GitLab Differentiators

| Differentiator |  Value  |  Proof Point |
|----------|-------------|------|
| 1) Leading SCM and CI in one application |  GitLab enables streamlined code reviews and collaboration at proven enterprise scale, making development workflows easier to manage and minimizing context switching required between tools in complex DevOps toolchains. Users can release software faster and outpace the competition with the ability to quickly respond to changes in the market. | Forrester names GitLab among the leaders in [Continuous Integration Tools in 2017](/analysts/forrester-ci/), Alteryx uses GitLab to have code reviews, source control, CI, and CD [all tied together](/customers/alteryx/). Axway [overcomes legacy SCM and complex toolchain](/customers/axway-devops/). |
| 2) Rapid innovation |  GitLab embraces an approach to rapid innovation that organizations undergoing digital transformations or adopting CI/CD also work diligently to implement internally with frequent, regular, releases delivered on a monthly basis. This provides end users with the latest and greatest in terms of capabilities/features, consistent security updates, and other incremental value adds over time. By GitLab "walking the talk" in regards to CI/CD, we understand the needs and pains that our users and organizations using GitLab face and share a mutual benefit from fully embracing this model centered around continuous improvement. | GitLab deploys over 160 times a day and is one of the [30 Highest Velocity Open Source Projects](/blog/2017/07/06/gitlab-top-30-highest-velocity-open-source/) from the CNCF, we're voted as a [G2 Crowd Leader 2018](/is-it-any-good/#gitlab-has-been-voted-as-g2-crowd-leader) with more than 170 public reviews and a 4.4 rating noting "Powerful team collaboration for managing software development projects," and have over 2,900 active contributors. |
| 3) Built in security and compliance | GitLab comes with security features out-of-the-box and automated security testing with audit controls to facilitate policy compliance. Moving security testing farther left into the SDLC catches potential problems earlier and shortens feedback loops for developers. This means a faster time to market delivering secure, compliant, code and an increase in customer confidence. | Gartner mentions GitLab as a vendor in the Application Monitoring and Protection profile in its [2019 Hype Cycle for Application Security](https://www.gartner.com/en/documents/3953770/hype-cycle-for-application-security-2019). GitLab positioned in the [niche players quadrant of the 2020 Gartner Magic Quadrant for Application Security Testing](https://about.gitlab.com/press/releases/2020-05-11-gitlab-positioned-niche-players-quadrant-2020-gartner-magic-quadrant-application-security-testing.html). Wag! takes advantage of [built-in security and faster releases with GitLab](/blog/2019/01/16/wag-labs-blog-post/), and Glympse makes their [audit process easier and remediates security issues faster](/customers/glympse/). |

## [Message House](./message-house/)

The message house provides a structure to describe and discuss the value and differentiators for Continuous Integration with GitLab.

## Customer Facing Slides

<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRmT3xh0VnNckhZOKRJz1x02tfY90ySaYb48YM55HInYMWa8fmSugK6lknvTChiNWSyYgy4ngK9FK3B/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>

### Discovery Questions

The sample discovery questions below are meant to provide a baseline and help you uncover opportunities when speaking with prospects or customers who are not currently using GitLab for CI. See a more complete list of questions, provide feedback, or comment with suggestions for [GitLab's CI discovery questions](https://docs.google.com/document/d/12NJZZr4A_CQWlODNc2JMWc1Gqtt2_uelPYLdT-VAm-M/edit?usp=sharing) and feel free to contribute!

#### Sample Discovery Questions

* CI/CD tool sprawl is one of the most common problems we see. How do you manage the complexities of many different teams using various tools and still meet their needs?
* Has there been any discussion to standardize on a single solution for CI since you’re already using GitLab for other needs?
* How are you currently supporting CI internally? Do you have a dedicated team or require in-house expertise for guidance, best practices, and fixing issues?
* How often is your day to day or planned work interrupted to fix or maintain your CI tool?
* Some of the teams I talk with spend hours 'babysitting' their pipeline jobs - has that been the case for you or your teams?
* Is your organization investing to improve CI/CD in the short term or long term? Is there a clearly defined strategy or timeline?
  * What’s the expectation on your team to support or facilitate this initiative?
  * Are you going to be onboarding additional teams in the next say, 12 months?
* Have your teams run into roadblocks or hurdles automating builds/tests at scale? Do you see any degradation in velocity as you scale?
* What is the workflow if a developer wants to create a new pipeline or add a job to an existing pipeline? How much time does that take the developer away from doing “real work”?

#### Additional Discovery Questions
* Has there been any discussion to standardize on a single solution for CI since you’re already using GitLab for other needs?
* What is your strategy around improving CI/CD?
* Would it be valuable to have your CD solution use the same configuration and format as your CI, AND have visibility into the full product pipeline from idea to production?
* Tell me about the difficulties you're having managing complex pipelines and supporting integrations.

#### Jenkins-specific Discovery Questions
Please see the [Jenkins-specific questions](https://docs.google.com/document/d/1g0ftF3kSQ0_OUpvuM4WUseFUjd_iSsPXQoIqKR7Ledg/edit)  *(GitLab internal only)*

### Industry Analyst Relations (IAR) Plan

- The IAR Handbook page has been updated to reflect our plans for [incorporating Use Cases into our analyst conversations](https://about.gitlab.com/handbook/marketing/product-marketing/analyst-relations/#how-we-incorporate-use-cases-into-our-analyst-conversations).
- For  details specific to each use case, and in respect of our contractual confidentiality agreements with Industry Analyst firms, our engagement plans are available to GitLab team members in the following protected document: [IAR Use Case Profile and Engagement Plan](https://docs.google.com/spreadsheets/d/14UthNcgQNlnNfTUGJadHQRNZ-IrAe6T7_o9zXnbu_bk/edit#gid=0).

For a list of analysts with a current understanding of GitLab's capabilities for this use case, please reach out to Analyst Relations via Slack (#analyst-relations) or by submitting an [issue](https://gitlab.com/gitlab-com/marketing/product-marketing/issues/new) and selecting the "AR-Analyst-Validation" template.

## Competitive Comparison
Amongst the many competitors in the DevOps space, [Jenkins](/devops-tools/jenkins-vs-gitlab.html) and [CircleCI](/devops-tools/circle-ci-vs-gitlab.html) are the closest competitors offering continuous integration capabilities.

## Proof Points - Customer Recognitions

### Quotes and reviews

#### Gartner Peer Insights

*Gartner Peer Insights reviews constitute the subjective opinions of individual end users based on their own experiences, and do not represent the views of Gartner or its affiliates. Obvious typos have been amended.*

>"We've migrated all our products from several "retired" VCS's to GitLab in only 6 months. - Including CI/CD process adoption - Without [loss] of code - Without frustrated developers - Without enormous costs"
>
> - **Application Development Manager**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1037269)
>
>"Finally, the most amazing thing about GitLab is how well integrated the [GitLab] ecosystem is. It covers almost every step of development nicely, from the VCS, to CI, and deployment."
>
> - **Software Engineer**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1038051)
>
>"One of the best ways to approach source code management (Git) and release pipeline management [CI/CD]. [Gitlab] gives you a simple yet highly customizable approach to release management which is a complicated topic."
>
> - **System Engineer**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1045457)
>
>"The best software tool to manage all our projects and configure [CI/CD]. I will definitely recommend GitLab to everyone that wants to start a new project and doesn't want to use too many tools, GitLab has everything that you need to manage."
>
> - **Web Developer**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1063698)
>
>"Over the years, we have successfully taken advantage of [Gitlab's] continuous deployment and integration mechanisms to build [and] maintain a robust and solid codebase."
>
> - **Co-Founder/CEO**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1111080)
>
>"One of the best [tools] for continuous integration and continuous deployment. "
>
> - **Lead - Mobile Apps**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1117401)
>
>"Overall, the experience with GitLab is very positive, it provides many powerful features especially regarding Continuous Integration and [Continuous Deployment]"
>
> - **Developer Analyst**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1140016)
>
>"[GitLab's] UI is so easy and manageable to understand. Built-in continuous integration is one of its best [features]."
>
> - **Technical Evangelist**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1092444)
>
>"GitLab isn't just for hosting your code, it's for the entire lifecycle of your code. Since they host code, it makes sense for them to provide services around development and getting code into production. Their integration into other services is really easy. They give you GitLab-CI for any CI/CD needs, driven from a yaml file."
>
> - **Testing Analyst**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/886423)
>
>"This is really an amazing source code repository toolset which enables the robust CI practices which are inbuilt features of GitLab. This can be utilized well in any enterprise looking for the smooth CI/CD pipelines."
>
> - **Software Engineer**, [Gartner Peer Review](https://www.gartner.com/reviews/market/enterprise-agile-planning-tools/vendor/gitlab/product/gitlab/review/view/1009762)

### Blogs

#### [Jaguar Land Rover](/blog/2018/07/23/chris-hill-devops-enterprise-summit-talk/)

* **Problem:** JLR had **6 week feedback loops** resulting in slowdowns
* **Solution:** Premium (CI)
* **Result:** **Feedback loops reduced to 30 mins.** JLR is deploying within the engineering environment, 50-70 times per day of each individual piece of software to a target or to a vehicle.
* **Sales Segment:** Enterprise

#### [Ticketmaster](/blog/2017/06/07/continous-integration-ticketmaster/)

* **Problem:** Long Jenkins build times slowed down CI pipeline
* **Solution:** Premium (CI)
* **Result:** Less than 8 minutes total from commit to build, test and generate artifacts
* **Sales Segment:** Enterprise


### Case Studies

#### [Goldman Sachs](/customers/goldman-sachs/) 

* **Problem** Needed to increase developer efficiency and software quality 
* **Solution:** GitLab Premium (CI/CD, SCM) 
* **Result:** Improved from **1 build every two weeks to over a 1000/day**, or releasing 6 times per day per developer, and an average cycle time from branch to merge is now 30 minutes; simplified workflow and simplified administration
All the new strategic pieces of ’software development platforms are tied into GitLab. GitLab is used as a complete ecosystem for development, source code control and reviews, builds, testing, QA, and production deployments.
* **Sales Segment:** Enterprise

#### [Hotjar](/customers/hotjar/) 

* **Problem** Hotjar was looking to replace Jenkins and reduce complexity within their tool stack 
* **Solution:** GitLab Silver (CI/CD) 
* **Result:** Time of CI builds **decreased by 30%**, improved to 2-15 deploys per day, and 50% deployment time saved.
* **Sales Segment:** SMB

#### [CERN](/customers/cern/)

* **Problem:** CERN was looking for an open source way to host their pipelines
* **Solution:** GitLab Starter (CI)
* **Result:** The European-based particle physics laboratory, uses GitLab for more than **12,000 users and 120,000 CI jobs a month**
* **Sales Segment:** Enterprise

#### [Wish](https://about.gitlab.com/customers/wish/)

* **Problem:** Wish was using TeamCity for CI and build management, but their automated jobs kept failing. The development team used a homegrown toolchain that included GitHub for SCM and Phabricator for code reviews. 
* **Solution:** GitLab Premium (CI)
* **Result:** **50% reduction in pipeline down time.** Wish has removed the bottlenecks associated with complex toolchains.
* **Sales Segment:** Mid-Market

### References to help you close
[SFDC report of referenceable Verify customers](https://gitlab.my.salesforce.com/a6l4M000000kDwk) Note: Sales team members should have access to this report. If you do not have access, reach out to the [customer reference team](https://about.gitlab.com/handbook/marketing/product-marketing/customer-reference-program/#which-customer-reference-team-member-should-i-contact) for assistance.

Request reference calls by pressing the "Find Reference Accounts" button at the top of your stage 3 or later opportunity.

## Adoption Guide

The following section provides resources to help TAMs lead capabilities adoption, but can also be used for prospects or customers interested in adopting GitLab stages and categories.

### Playbook Steps

1. Ask Discovery Questions to identify customer need
2. Complete the deeper dive discovery sharing demo, proof points, value positioning, etc.
3. Deliver [pipeline conversion workshop](https://about.gitlab.com/handbook/customer-success/playbooks/ci-verify.html) and user enablement example
4. Agree to adoption roadmap, timeline and change management plans, offering relevant services (as needed) and updating the success plan (as appropriate)
5. Lead the adoption plan with the customer, enabling teams and tracking progress through engagement and/or telemetry data showing use case adoption

### Adoption Recommendation

This table shows the recommended use cases to adopt, links to product documentation, the respective subscription tier for the use case,  and telemetry metrics.

| Feature / Use Case                                           | F/C  | Basic | S/P  | G/U  | Telemetry | Notes |
| ------------------------------------------------------------ | ---- | ----- | ---- | ---- | --------- | ---- |
| [Try Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/#quick-start) |  x    |   x    |   x   |  x    |           |     |
| [Enable shared runners](https://docs.gitlab.com/ee/ci/runners/) |   x   |   x    |   x   |   x   |           |
| Enable [container registry](https://docs.gitlab.com/ee/administration/packages/container_registry.html#enable-the-container-registry) across instance |  x    |   x    |  x   |   x   |           | GitLab.com container registry enabled at [Group level](https://docs.gitlab.com/ee/user/packages/container_registry/index.html#control-container-registry-for-your-group)    |
| Build [Instance Template Repository](https://docs.gitlab.com/ee/user/admin_area/settings/instance_template_repository.html) |      |       |   x (Premium only)   |  x (Ultimate only)    |           |     |
| Create [custom instance-level project templates](https://docs.gitlab.com/ee/user/admin_area/custom_project_templates.html) |      |       |   x (Premium only)   |  x (Ultimate only)   |           | GitLab.com can utilize [group-level project templates](https://docs.gitlab.com/ee/user/group/custom_project_templates.html)    |
| Add.gitlab-ci.yml file to your repositories root directory   |  x    |   x    |    x  |   x   |           |     |
| [Convert declarative Jenkinsfiles](https://docs.gitlab.com/ee/ci/jenkins/#converting-declarative-jenkinsfiles) *(Jenkins conversion only)* |  x    |    x   |   x   |  x    |           |     |
| **Run your pipeline!** CI/CD examples can be [viewed here](https://docs.gitlab.com/ee/ci/examples/README.html) |   x   |   x    |  x    |  x    |           |     |
| Make use of GitLab’s [CI feature index](https://docs.gitlab.com/ee/ci/README.html#feature-set) |  x    |   x    |   x   |   x   |           |     |

The table includes free/community and paid tiers associated with GitLab's self-managed and cloud offering.

- F/C = Free / Core
- Basic = Bronze/Starter
- S/P = Silver / Premium
- G/U = Gold / Ultimate

#### Additional Documentation Links

- [Introduction to CI/CD with GitLab](https://docs.gitlab.com/ee/ci/introduction/)
- [Getting started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/)
- [GitLab CI/CD Examples](https://docs.gitlab.com/ee/ci/examples/)
- [Migrating From CircleCI to Gitlab](https://docs.gitlab.com/ee/ci/migration/circleci.html)
- [Migrating from Jenkins to GitLab](https://docs.gitlab.com/ee/ci/jenkins/#migrating-from-jenkins)

### Enablement and Training

The following will link to enablement and training videos and content.

- [Make Your Life Easier with CI/CD Presentation](https://docs.google.com/presentation/d/1scYkmV4Xdfj-8iwwpEiLCe0vBfpAdrL5pyA2w1Fgnf0/edit#slide=id.g7193b194b5_0_96)
- [CI/CD Overview Video](https://www.youtube.com/watch?v=wsbSvLyC2Z8)
- [CS Skills Exchange: CI Deep Dive](https://www.youtube.com/watch?v=ZVUbmVac-m8&list=PL05JrBw4t0KorkxIFgZGnzzxjZRCGROt_&index=3&t=0s)
- [CS Skills Exchange: Runners Overview](https://www.youtube.com/watch?v=JFMXe1nMopo&list=PL05JrBw4t0KorkxIFgZGnzzxjZRCGROt_&index=11&t=0s)
- [CS Skills Exchange: Runners Overview](https://www.youtube.com/watch?v=JFMXe1nMopo&list=PL05JrBw4t0KorkxIFgZGnzzxjZRCGROt_&index=11&t=0s)
- [Technically Competing Against Microsoft Azure DevOps](https://drive.google.com/open?id=18jwSeeUylGXv8LoEedCMRfBZt9t7QLOYKCHJp-SvdqA) *(GitLab internal only)*
- [Competing Against Jenkins](https://drive.google.com/open?id=1IvftLfaQyKn5-n1GLgCZokOoLU-FFzQ8LfJ9cf0FVeg) *(GitLab internal only)*
- *Coming soon.... CI Learning Path*

### Professional Service Offers

GitLab offers a [variety of pre-packaged and custom services](https://about.gitlab.com/services/) for our customers and partners. The following are service offers specific to this solution. For additional services, see the [full service catalog](https://about.gitlab.com/services/catalog/).

- [Gitlab CI Training](https://about.gitlab.com/services/education/gitlab-ci/)
- [Jenkins Migration Services](https://about.gitlab.com/services/migration/jenkins/)
- [Devops Fundamentals Training](https://about.gitlab.com/services/education/devops-fundamentals/)

## Key Value (at tiers)

### Premium/Silver
- Describe the value proposition of why Premium/Silver for this Use Case

### Ultimate/Gold
- Describe the value proposition of why Ultimate/Gold for this Use Case

## Resources

### What is CI/CD?

Check out this introductory video to learn the basics of CI/CD as software development best practices and how they apply with GitLab CI/CD!
<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/nLwJtVWXN70" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

### Presentations
* [Why CI/CD?](https://docs.google.com/presentation/d/1OGgk2Tcxbpl7DJaIOzCX4Vqg3dlwfELC3u2jEeCBbDk)

### Continuous Integration Videos
* [CI/CD with GitLab](https://youtu.be/1iXFbchozdY)
* [GitLab for complex CI/CD: Robust, visible pipelines](https://youtu.be/qy8A7Vp_7_8)
* [How do Runners work?](https://youtu.be/IsthhMm64u8)

### Integrations Demo Videos
* [Migrating from Jenkins to GitLab](https://youtu.be/RlEVGOpYF5Y)
* [Using GitLab CI/CD pipelines with GitHub repositories](https://youtu.be/qgl3F2j-1cI)

### Clickthrough & Live Demos
* [Live Demo: GitLab CI/CD Deep Dive](https://youtu.be/pBe4t1CD8Fc)
