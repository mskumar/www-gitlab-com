---
layout: handbook-page-toc
title: "Global Content Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What does the Global Content Team do?

The Global Content section includes the content marketing, editorial, and digital production teams. It is responsible for content strategy, development, and operations, including the stewardship of GitLab's audiences, users, customers, and partners' content needs. The Global Content Team creates engaging, inspiring, and relevant content, executing integrated content programs to deliver useful and cohesive content experiences that build trust and preference for GitLab.

### Quick links

- [GitLab blog handbook](/handbook/marketing/blog)
- [Content Marketing team handbook](/handbook/marketing/growth-marketing/content/content-marketing/)
- [Digital Production team handbook](/handbook/marketing/growth-marketing/content/digital-production/)
- [Editorial team handbook](/handbook/marketing/growth-marketing/content/editorial-team)
- [Gated content process](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/)
- [Newsletter process](/handbook/marketing/marketing-sales-development/marketing-programs/#newsletter)

## FY21-22 Direction

Embrace a media company mindset to develop a bingable content strategy that organically attracts and retains our target audience over the use of traditional disruption tactics with the goal of increasing website traffic and preference for our content resources. Our global content strategy prioritizes the needs and preferences of our audiences over selling GitLab in order to build trust and increase audience retention. We aim to create emjoyable content experiences that people seek out. 

We accomplish this through: 

1. Rigorous research and understanding of our audience needs, behaviors, and lived experiences. 
1. A single content hub that is easy to find, search, and interact with. 
1. Consistency in quality and format. Our audience knows what to expect. 
1. A mix of journalistic and entertainment style content. 
1. Exceptional communication standards. We prioritize a clear and direct tone of voice over cutsy, convoluted language. 

### Content Strategy Pillars 

1. **Audience-first mindset.**  All content is produced with the audience in mind and have a clearly definited "who, what, why, how". 
1. **Entertain and connect.** Our content is entertaining, enjoyable to consume, and expresses our acute understanding our audience. We want our content to compell our audience to binge and share versus bookmark for later. 
1. **Make it easy.** Relevant content is easy to find regardless of format. Our content finds our audiences where they are. 
1. **Media company mindset.** Our global content team operates with the mindset that our audience is our customers, content is our product, and subscribers are our currency. 

### Mission 

TODO: UPDATE Our content mission statement mirrors our [company mission](/company/strategy/#mission). We strive to foster an open, transparent, and collaborative world where all digital creators can be active participants regardless of location, skillset, or status. This is the place we share our community's success and learning, helpful information and advice, and inspirational insights.

### Vision

TODO: UPDATE Our vision is to build the largest and most diverse community of cutting edge co-conspirators who are defining and creating the next generation of software development practices. Our plan to turn our corporate blog in to a digital magazine will allow us to add breadth, depth and support to our participation in and coverage of this space.

### Teams

#### Content Marketing

The Content Marketing team is comprised of content marketers who specialize in different subject areas: Development, Operations, Security, and Customers. This team is responsible for content strategy and new content creation for our content pillars and integrated campaigns. They focus on developing consistent content journey experiences and primarily create resource-driven content such as case studies, eBooks, explainer videos, infographics, reports, whitepapers, and web articles. The goal of the Content Marketing team is to attract interest and position GitLab as a trusted resource through informational and educational content. 

Contact the Content Marketing team if you need assistance with:

1. Content planning or creation for a campaign 
1. Copywriting for a campaign 
1. Editing and/or review of long-form content (eBook, whitepaper, report)
1. Landing page copywriting for a gated asset 
1. New gated content or web article content idea 
1. Nurture email copywriting 

Visit the [Content Marketing Handbook page](/handbook/marketing/growth-marketing/content/content-marketing/) to learn more about the Content Marketing team and instructions on how to request help. 

#### Digital Production 

The Digital Production team is comprised of video/audio editors and producers who manage GitLab's digital content presence. This team is responsible for video marketing strategy (including GitLab's branded YouTube channel), podcasting, and digital media production. They primarily focus on storytelling by creating entertaining video and audio content in order to build an emotional connection with audiences. The goal of the Digital Production team is to build trust and an emotional connection between GitLab and our audiences.

Contact the Digital Marketing team if you need assistance with: 

1. Approval to post a video on GitLab's branded YouTube channel
1. Branding elements (video bumpers, lower thirds, etc.) for a video
1. New video or podcast idea
1. Review a video for brand compliance 
1. Video editing
1. Video production

Visit the [Digital Production Handbook page](/handbook/marketing/growth-marketing/content/digital-production/) to learn more about the Digital Production team and instructions on how to request help.

#### [Editorial](/handbook/marketing/growth-marketing/content/editorial-team) 

The Editorial team is comprised of content editors who manage the GitLab blog. 

## Editorial calendar

**Check the [FY21 Global Content Epic](https://gitlab.com/groups/gitlab-com/-/epics/229) for more details on our plans and activities for FY21.**

<iframe src="https://calendar.google.com/calendar/embed?src=gitlab.com_ame9843c6094ffc475vea9ftn4%40group.calendar.google.com&ctz=America%2FDenver" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>

### Adding an event to the editorial calendar

1. Make sure there is an associated issue for the event. Link the issue in the event description.
1. Always check "all day event" unless there is a specific time the piece should publish. This is rare and usually only if a blog post is tied to a time-sensitive PR announcement.
1. Name your event using the following nomenclature: <emoji, title of content, campaign>. Example: `📝 5 Reasons to Use GitLab, Just Commit`

**Legend**

- 📣 Announcement
- 📝 Blog post
- 📚 eBook/Whitepaper
- ℹ️ Infographic
- 🎙️ Podcast
- 📈 Report
- 📹 Video
- 🌐 Webpage

## Content team responsibilities

- Content strategy
- Content operations
- Content governance
- Content pillar planning & execution
- Customer content creation
- Digital media production (videos, podcasting, event A/V)
- Blog management including writing, editing, and scheduling
- Branded YouTube management

## Communication

Please use the following Slack channels:

- `#content` for general inquiries
- `#content-updates` for updates and log of all new, published content
- `#content-hack-day` for updates and information on Content Hack Day
- `#blog` RSS feed of published posts

### Issue trackers
- [Global Content](https://gitlab.com/groups/gitlab-com/-/boards/1500703?&label_name[]=Global%20Content)
 - [Blog](https://gitlab.com/gitlab-com/www-gitlab-com/boards/804552?&label_name[]=blog%20post)
 - [Content by stage](https://gitlab.com/groups/gitlab-com/-/boards/1136104)
 - [Content Marketing](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/boards/911769?&label_name[]=Content%20Marketing)
 - [Digital production](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/boards/1120979?&label_name[]=Video%20project)

### Labels

We use the [mktg-status:: labels](/handbook/marketing/#boards-and-labels) to work status (triage, plan, WIP, design, review, scheduled).

### GitLab.com group labels

- `Corporate Marketing`: Department label. Add this label to all issues and epics.
- `Global Content`: General team label to track all issues related to the content team. Add this label to all issues and epics.
- `New Content Request`: Indicates a request for content creation, editing, or review.
- `Stage::Awareness`: Indicates content creation for the awareness stage of the buyer's journey.
- `Stage::Consideration`: Indicates content creation for the consideration stage of the buyer's journey.
- `Stage:: Purchase`: Indicates content creation for the purchase stage of the buyer's journey.
- `Stage:: Technical User`:
- `Editorial`: General label to track all issues related to the editorial team. This brings the issue in the Blog issue board for actioning.
- `Content Marketing`: Used by the Content Marketing team as a general label to track all issues related to content marketing. This brings the issue into the board for actioning.
- `Content Pillar`: Used by the Content Marketing Managers to indicate a content pillar epic. This label should **only be used on pillar epics.**
- `Gated Content`: Indicates content that requires MPM support. Use this label when creating a new epic for gated content (i.e. eBook, whitepaper, report).
- `Digital Production`: Used by the Content Marketing team as a general label to track all issues related to content marketing. This brings the issue into the board for actioning.
- `Blog UX`: Used to indicate a proposed change to the blog user experience.
- `Technical Post`: Indicates a blog post covering a technical engineering angle.

### www-gitlab-com project labels

- `Blog Post`: General label for blog posts. Add this label to all blog post issues and merge requests.
- `Blog Priority`: Indicates a blog post that is a high priority. These pitches and posts should be followed up on immediately.
- `Blog::Pitch`: Use this label when pitching a new blog post idea. All blog post issues must start here. Blog issues do not leave this stage until they have been assigned to a content team member.
- `Blog::Planning/In Progress`: Indicates blog posts that have been triaged to a content team member to work on.
- `Blog::Review`: Indicates blog posts that are ready for a content team review.
- `Blog::Scheduled`: Indicates blog posts that have been reviewed and are scheduled for publishing by the Managing Editor.
- `Blog::Waiting for author`: Indicates blog posts that have been reviewed by content team and are waiting for the author to address feedback or approve for scheduling.
- `CEO Interview`: Blog posts suggested by the CEO and need immediate action.
- `Error budget S1`: Indicates a blog post that has incurred 15 error budget points for providing than 2 working days' notice for a time-sensitive post.
- `Error budget S2`: Indicates a blog post that has incurred 10 error budget points for failure to submit complete, formatted MR a minimum of 2 working days ahead of publish date.
- `Error budget S3`: Indicates a blog post that has incurred 5 error budget points for submitting an MR without all required formatting, links, and images included.
- `Customer interview`: Use for blog posts that require or include a customer interview.
- `External outlet`: Use to indicate an article will be published outside of GitLab web domain.
- `Guest/Partner post`: Indicates a blog post that is being written and submitted from someone outside of GitLab.
- `Remote work post`: Indicates a blog post on the topic of remote work.
- `Sensitive`: Indicates a blog post with the potential to have wide-spread customer or community impact.
- `Unfiltered`: Indicates a blog post to be published the GitLab unfiltered blog.


## Requesting content team support
*Need help finding relevant content to use in an email or to send to a customer?* Ask for help in the #content channel.*

##Requesting content and copy reviews

1. If the content you're looking for doesn't exist and it's a:
   1. Blog post: See the [blog handbook](/handbook/marketing/blog)
   1. Digital asset (eBook, infographic, report, etc.): open an issue in the [corporate marketing project](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/) and label it `content marketing`
   1. Video: open an issue in the [corporate marketing project](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/) and label it `video production`
1. If you need a copyedit, ping @erica. Please give at least 3 days' notice.

## Content production

The content team supports many cross-functional marketing initiatives. We aim for content production to be a month ahead of planned events and campaigns so that content is readily available for use. In accordance with our values of [iteration](/handbook/values/#iteration) and [transparency](/handbook/values/#transparency), we publish our proposed content plans at the beginning of each quarter. We don't hoard content for one big launch and instead plan sets of content to execute on each quarter and published each piece of content as it's completed.

Content production is determined and prioritized based on the following:

1. Corporate GTM themes
1. Integrated campaigns
1. Corporate marketing events
1. Newsworthiness
1. Brand awareness opportunity

We align our content production to pillars and topics to ensure we're creating a cohesive set of content for our audience to consume. Pillar content is multi-purpose and can be plugged into integrated campaigns, event campaigns, and sales plays.

### Content alignment

Content production is aligned to digital campaigns and product messaging.

**Definitions**

- Campaign/Value driver: A high-level GTM message that doesn't change often. Campaigns are tracked as `Parent Epics`.  
- Pillar: A story within a theme. Pillars are tracked as `Child Epics`.
- Set: A topical grouping of content to be executed within a specific timeframe. Sets are tracked as `Milestones`.
- Resource: An informative asset, such as an eBook or report, that is often gated.

## Content stage / buyer's journey definitions

### Awareness (Top)
Users and buyers realize that they have a problem or challenge which could be solved through some sort of outside software or service.  At this stage, they are trying to define the scope and the relative impact and size of the problem.  They probably do not yet have a solution identified, but are in the process of learning about potential solutions to their specific business problem.

In general, messaging in collateral and content should be focused on **educating them about the problems they are facing, the business impact of their problems, and the reality that others are successfully solving the same problem**.  This is an opportunity for them to learn that GitLab is an authority in addressing their domains.

### Consideration (Middle)
Users and buyers understand the problem they are trying to solve and the business impact/value of addressing the problem and are now actively seeking and evaluating potential remedies to their business issue.  Typically, they will be considering a range of approaches from better leveraging existing tools to investing in new technologies and vendors.  In this stage they are focused on identifying options that meet their specific requirements and needs.  While cost will be a consideration, they have not yet made a final decision about how to address their needs.  In this stage, they will be defining their budget and overall plans for implementing a solution.

In general, collateral and content designed to reach prospects in this stage of their journey should be focused on **positioning GitLab as a viable and compelling solution to their specific problem**.  

### Decision/Purchase (Bottom)
Users and buyers have concluded that they need to invest in solving a specific business problem and are now comparing and evaluating specific options.  In this stage, they are evaluating and comparing different options in order to identify the ideal solution for their specific situation.  They will consider Fit (technology, process, etc), implementation effort/cost, total cost of ownership and other factors to guide them in making a final selection.  

In general, collateral and content designed to reach prospects in this stage of their journey should be focused on key information that a buyer needs to **justify GitLab as their chosen solution**.

## Content library

Content for us is stored in PathFactory. To request access to PathFactory, submit an access request form.

The content library in PathFactory can be filtered by content type, funnel stage, and topic. Topics are listed and defined in the [marketing operations](/handbook/marketing/marketing-operations/pathfactory/).

Published content should be shared to the #content-updates channel and added to PathFactory with the appropriate tags.
