---
layout: markdown_page
title: Financial Wrap-up
category: Internal
---

Once the SOW items have been delivered in full and the project sign off has been received or passive acceptance:

1. Attach the signed Project sign off document or attach the email where the Project sign off document was sent to the customer via email in the SalesForce opportunity.
1. At the top of the page, in the post freeform box, chatter Andrew Murray, Siddharth Malik, Sarah McCauley, and Anna Piaseczna and add a note outlining if we got a signed Project Sign Off, acceptance was given via email, or if we have passive acceptance and then copy in the URL.  Hit share.

