---
layout: handbook-page-toc
title: "Using Timeline"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Timeline Notes

Meeting/call notes are the running notes for the TAM’s regular interaction with the customer. The call notes should at least cover the following topics:

- Updates on action items from the TAM
- New updates from the customer
  - Architectural changes
  - New deployments
  - Product feedback
- A review of any open support tickets in Zendesk
- Q&A
- Current customer health from a business and operational standpoint

Gainsight Activity types include:

- Update - General update on the customer, could be from an internal conversation
- Call - An audio/video call with the customer
- Email
- Verified Outcome - A Verified Outcome is where the customer _positively_ confirms we achieved their Success Plan Objective
- Milestone - This tracks when the customer achieves a major milestone such as Onboarding, time to value, TAM transition, etc.
- In-Person Meeting

##### How to Log Activities

To log activities (calls, meetings, emails, updates...), you'll primarily do it through:

1. Navigating to the customer
1. Clicking Timeline at the top
1. Hitting "+ Activity" and follow the rest of the steps

The other options to log activities are (1) on the Scorecard while recording TAM Sentiment or Product Risk or (2) on the Success Plan to log a Timeline activity specific to the plan.

Attendees will only appear if they are a) a Salesforce user for internal attendees, or b) a contact in the Salesforce account record. If your internal attendee does not have a SFDC account (e.g. Support Engineers or Product Designers), you do not need to log them and can just mention in the notes they were there. If your external attendee is not populating, make sure that they are added to the correct account (child accounts have different contact lists than their parent accounts), and if not feel free to add them by clicking the "Add Contact" button in Salesforce and inputting the required details. New SFDC contacts most likely won't populate in Gainsight until the following day, so this is a great opportunity to create a CTA for yourself!

If you would like to see the activity logging process in action, please watch the [enablement video that covers logging](https://youtu.be/PL9shBdCMmo).

## Customer Cadence Calls

One of the primary tools TAMs have to assess account health, and take steps to improve a weaker health score, is the customer cadence. This is an opportunity for the TAM (and other GitLab team members that the TAM feels should be included) and the customer team to sync on business outcomes, priorities, progress on initiatives, and concerns.

* Enterprise: Customer cadence calls should be scheduled weekly during the customer onboarding phase and at least monthly otherwise. 
* Mid-Market: Customer cadence calls should be scheduled frequently during the customer onboarding phase and at least quarterly otherwise. 

### Cadence Calls: After the Call

At the end of each customer call any changes to customer health should be reflected in the customer's. You have a few ways to update the TAM Sentiment and Product Sentiment for an account's health score, described in [Determining TAM Sentiment and Product Risk](/handbook/customer-success/tam/health-score-triage/#determining-tam-sentiment-and-product-risk).

If the Customer Health Score drops to Yellow or Red, [follow the triage process](/handbook/customer-success/tam/health-score-triage/#account-triage-project).

In addition to Timeline notes in Gainsight, call notes should be [saved in Google Drive following this format](https://drive.google.com/drive/folders/0B-ytP5bMib9Ta25aSi13Q25GY1U):

`/Sales/Customers & Prospects/A/Acme/Acme - Meeting Notes`

[See an example meeting notes here](https://docs.google.com/document/d/1dAcHBqoRTY6qqSw27VQstCCnk5Fxc2oIsbpKs014h3g).

The rationale for saving call notes in this manner is as follows:

- Call notes frequently contain sensitive information and are for the internal sales team and management to review.
- Call notes are tightly linked to the Health Score and should be available in the same “pane of glass” as the Health Scorecard.
- Access to Gainsight is limited to TAMs, so other members of the Sales and Customer Success organizations will look for notes in Google Drive.
- A folder structure allows non-Customer Success executives and support staff to easily locate the notes in the case of an escalation.
- The naming convention “&lt;customer&gt; - Meeting Notes” allows for fast searching using [Google Cloud Search for Work](https://cloudsearch.google.com/)


##### BCC'ing Emails

Similar to BCC'ing emails to Salesforce, you can also do the same with Gainsight. To get your personalize email address, navigate to your settings:

1. Click the person icon in the upper right
1. Click "My Settings"
1. Navigate to "Inbound Emails" and copy that email address (PS: save it for easy reference)

NOTE: BCC'ing emails to Gainsight is _not_ a required step. However, if you want an email to appear in Gainsight and Salesforce, you need to BCC Gainsight.

1. Emails logged in Salesforce stay in Salesforce
1. Emails logged in Gainsight appear in Gainsight and then are synced to Salesforce during the nightly sync

##### Timeline View

When going to the timeline from the lefthand sidebar (not for a specific customer), you will see all timeline events for all TAMs. From there, you can filter by clicking the three horizontal lines to customize what events are shown (see picture below). For example, you can search for yourself as the author to find all timeline events you created, or you can search for a company to find all timeline events for a specific customer.

![Gainsight Timeline Filter](/images/handbook/customer-success/gainsight-timeline-filter.png "Gainsight Timeline Filter")
