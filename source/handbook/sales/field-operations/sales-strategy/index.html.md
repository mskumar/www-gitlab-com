---
layout: handbook-page-toc
title: "Sales Strategy"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Welcome to the Sales Strategy Handbook

### Charter

To drive sales success by providing data, reporting, analytics, and actionable insights to leadership across GitLab

### Working with Sales Strategy

Create an issue in the [analytics project](https://gitlab.com/gitlab-com/sales-team/field-operations/analytics/issues) and use the `Sales Strategy` label

### Team Members

| Team Member | Role | GitLab Handle |
| ------ | ------ | ------ | 
| Matt Benzaquen | Sr Manager Sales Strategy | [@mbenza](https://gitlab.com/mbenza) | 
| Swetha Kashyap | Sales Commission Manager | [@swethakashyap](https://gitlab.com/Swethakashyap) |
| John Mahdi | Sr Sales Analytics Analyst | [@jmahd](https://gitlab.com/JMahd) |
| Melia Vilain | Sr Sales Analytics Analyst | [@mvilain](https://gitlab.com/mvilain) |
| David Mack | Sr Sales Analytics Analyst | [@DavidMack](https://gitlab.com/DavidMack) |

### Sales Headcount Change Management Process

Please find instructions on how to initiate a Sales Headcount change [here](https://about.gitlab.com/handbook/sales/field-operations/sales-strategy/sales-headcount)
