---
layout: handbook-page-toc
title: Support Team Knowledge Areas
category: General
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Overview

The purpose of this page is to provide a reference of Support team members who feel that they are knowledgeable and willing to assist with tickets in a particular area.

Unlike the [team page](/company/team/), team members listed here may or may not be [an expert](/company/team/structure/#expert), such as [working on a bootcamp](https://gitlab.com/gitlab-com/support/support-training/-/tree/master/.gitlab/issue_templates) but not completed. Many areas however do not currently have a bootcamp, or their knowledge area is very specific, such as "GitLab.com SSO".

### When to Use

Team members should make use of existing resources before making use of this list: docs, ZenDesk, Slack (support, stage/group specific channels, questions).

If no response is forthcoming or you need to find someone to assist, you can consider pinging team members listed below.

### General Technologies and Topics

Please remember to post in the appropriate Slack channel (such as #kubernetes) and/or check the [team page](/company/team/) for non-support team members who can help as well.

Topics are in alphabetical order with team members grouped by region.

| Technology    | Region | Team Member     |
|---------------|--------|-----------------|
| Docker        | AMER | Harish <br> Cody West          |
| ElasticSearch | AMER | Blair  <br> Michael Lussier  <br>  JasonC <br> Eldridge|
| Git LFS       | AMER | Diana                |
| HA            | AMER | Aric <br> JamesM <br> JasonC |
| Kubernetes    | AMER | Harish <br> JasonC <br> Michael Lussier <br> Thiago |
| Linux         | AMER | Greg <br> Keven <br> Cody West |
| Omnibus       | AMER | Aric  <br> Diana <br> Eldridge <br> Greg <br> Harish <br> John <br> Nathan <br> Cody West <br> Keven |
|               | APAC | AlexS <br> Anton <br> Weimeng |
| Performance   | AMER | Will <br> Cody West |

### Product Stages

This section is ordered by stage and group according to the [product categories page](/handbook/product/categories/#devops-stages). If no one is listed for an area, look for a Support Counterpart on the product page.

#### Manage

##### Access

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
| LDAP                   | AMER | Blair <br> Diana <br> Harish <br> JasonC |
|                        | APAC | AlexT <br> Athar  |
| SAML                   | AMER | Blair <br> Diana <br> JasonC |
| SSO for Groups         | AMER | Cynthia <br> Tristan |

##### Import

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
| Project Import         | AMER | Cynthia |

#### Plan

##### Project Management

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
|  | | |

##### Portfolio Management

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
|  | | |

##### Certify

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
| Service Desk | Amer | John <br> Eldridge |

#### Create

##### Source Code

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
| Code Owners | AMER | Tristan |

##### Editor

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
| Snippets | AMER | John |
| Web IDE | AMER | John |

##### Gitaly

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
| Gitaly | AMER | Will? |

##### Ecosystem

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
| JIRA Integration | AMER | Tristan <br> Aric <br> Eldridge |

#### Verify

##### Continuous Integration

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
| CI | EMEA | DeAndre |
|    | AMER | Cynthia <br> Harish <br> Cody West |
|    | APAC | AlexT <br> Athar |
| Jenkins Integration | AMER | Aric <br> Eldridge |

#### Package

| Topic                  | Region | Team Member |
|-------------|------------------------|-------------|
| Package                | AMER | Thiago <br> Sara |

#### Release

##### Progressive Delivery

| Topic                  | Region | Team Member |
|-------------|------------------------|-------------|
|  | | |

##### Release Management

| Topic                  | Region | Team Member |
|-------------|------------------------|-------------|
| Release Orchestration | Amer | JamesM |
| Secrets Management | Amer | JamesM |
| Release Evidence | Amer | JamesM |
| GitLab Pages | Amer | John <br> Keven <br> JamesM|

#### Configure

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
|  | | |

#### Monitor

##### Health

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
|  | | |

#### Secure

| Group             | Topic                  | Team Member |
|-------------------|------------------------|-------------|
| Static Analysis   | SAST                   | AMER: <br> Thiago <br> Greg |
| Dynamic Analyasis | DAST                   | AMER: <br> Thiago <br> Greg |
| Composition Analysis | Dependency Scanning <br> Container Scanning <br> License Compliance | AMER: <br> Greg |

#### Defend

##### Container Security

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
|  | | |

#### Growth

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
| License and Renewals | AMER | Blair <br> Cynthia <br> David C <br> Harish <br> Keven
|                      | APAC | Jerome <br> Sameer  |
| Escalation Points      | EMEA | Donique |
|                        | AMER | Tom H |
|                        | APAC | Rotanak |

#### Enablement

##### Distribution

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
|  | | |

##### Geo

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
| Geo                    | EMEA | Alin <br> Catalin |
|                        | AMER | Aric <br> Harish <br> John <br> Eldridge |
|                        | APAC | Anton   |
