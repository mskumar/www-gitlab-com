---
layout: markdown_page
title: Responsible Disclosure Policy
---

Please email `security@gitlab.com` to report any security vulnerabilities in GitLab itself. Alternatively you may also send us your report via [HackerOne](https://hackerone.com/gitlab).

To report vulnerabilities in a GitLab.com-hosted project, please create a [confidential vulnerability submission](https://gitlab.com/gitlab-org/cves/-/issues/new?issue[confidential]=true&issuable_template=Manual%20Submission&issue[title]=Vulnerability%20Submission).

We will acknowledge receipt of your vulnerability report the next business day
and strive to send you regular updates about our progress. Our goal is to
determine if the vulnerability is valid or not and communicate back to you 
within 30 business days.

If the status of your disclosure is unclear, please feel free to ping us
(`@gitlab-org/secure/vulnerability-research`) within the created issue. If you
would prefer to contact us via encrypted email, download our key from
[MIT PGP key server](https://pgp.mit.edu/pks/lookup?op=get&search=0xFA6DEFCB219262C8)
or find it [below](#cve-public-gpg-key), and email us at `cve@gitlab.com`.

Please refrain from requesting compensation for reporting vulnerabilities. If you want we will [publicly acknowledge](/security/vulnerability-acknowledgements/) your responsible disclosure. We also try to make the confidential issue public after the vulnerability is announced, for an example see our [impersonation feature issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/15548). HackerOne also makes the bug reports [public after 30 days](https://hackerone.com/disclosure-guidelines) if neither party objects, for an example see [the report for a persistent XSS on public project page](https://hackerone.com/reports/129736).

You are not allowed to search for vulnerabilities on GitLab.com itself. GitLab
is open source software, you can install a copy yourself and test against that. You can either download [CE](/install/), [EE](/downloads-ee/), or the [GitLab Development Kit](https://gitlab.com/gitlab-org/gitlab-development-kit).
If you want to perform testing without setting GitLab up yourself please contact us to
arrange access to a staging server.

You can find more details on how we handle [security releases here](https://gitlab.com/gitlab-org/release-tools/blob/master/doc/security.md).

On our website you can find more about [the availability and security of GitLab.com](/gitlab-com/#faq). Security issues that are not vulnerabilities can be seen on [our public issue tracker](https://gitlab.com/gitlab-org/gitlab-ce/issues?label_name%5B%5D=security).

## Confidential issues

When a vulnerability is suspected or discovered we create a [confidential ~security issue](/handbook/engineering/security/#creating-new-security-issues) to track it internally.
Security patches are pushed to [dev.gitlab.org](https://dev.gitlab.org), which is not publicly accessible, and merged into the `security` branch.
They should not appear on [GitLab.com](https://gitlab.com) until the security release has been announced and updated packages are available.

Details can be found under [Critical Security Releases](https://gitlab.com/gitlab-org/release/docs/blob/master/general/security/process.md#critical-security-releases) in `release/docs`.

## Red Team Rules of Engagement

If you want to conduct red teaming against GitLab you will need written permission upfront.
You can apply by emailing security@gitlab.com your plans and experience.
You need to get a written authorization letter from our Directory of Security.
While you are engaged in red teaming activities you should coordinate with the Security Team so escalation (law enforcement, etc.) can be avoided.
The Security Team will notify the Infrastructure Team as well as the VP of Engineering so that awareness is maintained.

### Public GPG Key

 * `GitLab Security <security@gitlab.com>`
 * ID: A11CC6B586EF1357
 * Fingerprint BD6DA8467F8BA81203672C83A11CC6B586EF1357

```
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQENBFuql0YBCADpCTagFlvldEfM/yZaCZ8C/CxrqnOfdAK6FDqQrZpH/cvvoauL
W09qXXsi1yZOWOjbKX/ax7qw/7Z2aAzvWOW+epBfmA7lyJOwtQfF81wKkqPSF+tK
dlibgFX6QAgrR6G9IVOy72/MD/T2TnTL40zuYC3p23h7T5wkLqXUaHa9Fc3M2OOM
N4TcIxfz5ypgEgBbF/VwCgnmKyUWxy7AXmLwFywCRJY60zKf57OAxJPn+0XAwh34
JJKC+CTh15RAh/rh6Oh2ihvjltybJgThQp2F72jNAgzkAflwqQeF7psjKXQxvNR7
fQVGndHG7/H1HUoOSRsRHvgLtJCDVsILS6STABEBAAG0JUdpdExhYiBTZWN1cml0
eSA8c2VjdXJpdHlAZ2l0bGFiLmNvbT6JAVQEEwEIAD4WIQS9bahGf4uoEgNnLIOh
HMa1hu8TVwUCW6qXRgIbAwUJA8JnAAULCQgHAgYVCgkICwIEFgIDAQIeAQIXgAAK
CRChHMa1hu8TV2fKB/4ikVC42Xc/l5eh5TZMajYtr5C69bYdHOWkEtSCMXgp3x2X
Ezy59cTxWSsD76hTr2ubSDzDQZ21eFlq6mZ98hsz9+y64C/3bO/3LFF6itPamznS
oAYT9zFpNZVmeH4+0SLZBaDlsQOZ1EqjRGVySWRSCC/hnMfugCukEdQJrmq/QXvn
2D4/M3XEMHh/c2cExYYxafIuQ6zaxbISzfQOLFEZWNPk1foQVJd8XANYsglW8sdI
BczMAVNVzLZ6z1feD+cXf1N3SVUNpocMcQW7sWkqZhw0TsowrMKSuPMHcEzZLPlH
qs1meTUCWbMFLKwN1Byl5JOOTdKBwmp72oWOmDkYuQENBFuql0YBCAC7v0pbMG9u
jjoPrQMngmKD26sMMCj4Tz+pOWhpPgvthv+0ufgsAwFA+Oc19Xdt+MaOhwLfQp8R
meVpsxscZfG+2kzVpytl7edHpNxwy9vS1z7iTuEwBjAk0Fr9D1u7uUvYAEvkXXw6
2/WPZo95aSuTvKMussHUH1hxjDahHRyYn1j8Q6w7mVI9MtV8eCo2qpJjnJrIe3UK
LDyXU9fbZws1Fzv0f71VroXPOTOs7FjEiD7a8d6Y7d8zI8gInCfw+b5Te1Qt5BqW
eyRZpGtvWI1/gvOu8lCGa0FxXTHl+n3nQTbPruRC+lWWqC9uQwykCmewMilZA+A+
2/7xUCSPnVW9ABEBAAGJATwEGAEIACYWIQS9bahGf4uoEgNnLIOhHMa1hu8TVwUC
W6qXRgIbDAUJA8JnAAAKCRChHMa1hu8TV1QeB/4gWi/KjcXgrxt9paHuGHB46rRs
fowa9Q2EkeFPCSoUQGmOYN1En/Si8rZ/VZyszFvHAQD2a56UizSorOTAGNVG5jUK
1kaCalQiKhCKWf8oUS7Cu+GXS9ESZHa4dfHIx5w+qED9tM6Nd6pNK4v4JFPQwN/F
W4jkfRbwvreLreACHZ1OY2ZNhW8T2g8d3Vqv+D79BszIBzzpR0YGXmKqbI5zrxkU
jdfBHttzAYPDrBxvqAcXZF6vKXRupxyrmYBwkygMM2JX3YoU7echKqQuKnsJPWKp
6v8dhgsUbu6r2UoYWmk1ha7USM76xXbYc6buHDEjogoZF1ydDyQhB3Um4aFP
=LHOO
-----END PGP PUBLIC KEY BLOCK-----
```

### CVE Public GPG Key

 * `GitLab CVE <cve@gitlab.com>`
 * ID: FA6DEFCB219262C8
 * Fingerprint 8DEEE034553BF98F760CA9D2FA6DEFCB219262C8

```
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQINBF653qgBEACuTDL3NDNhJpXrZAb1eS1Lf29zGx5dXYSikjNEZiTlLJ12AYPC
x0bBxQnRrUuGvjWp1YKjKfD41cNXo5T3bjRmp3hFHJp9jahrbHmYP7etsoyxB+vu
h3h/ZimqqKzZpK0wMv8CCAEOgwnWOEaeI5ktiKrfV4BM3GcTiJ2a2rPwV/IkUc6I
K+pm5QEmqhrrfqP15qTs/CRC+vthaWz3IX9l1WgTo6y4nu2p6ESJdc+6W9Xe3Af2
EVu24m1gMgvbcay5vQpY5yH+9C1Bxs0V9155qE9EfWqKbWao/A+RGV/mbrguPwIX
BexBiu7GTjGUAsMpb129qUotwNfdLMawjFZ8TXq7HXewajspIiwpirbxWC9SqIRW
qKE6gxkxMcY+jhBatJr7nvrrHBRDx0EeL6MimDoeCDZZn1UNt03EqTin4gGPPtEa
fH1sLfcrDSOH3TSQb2X4K3jntstH/daV8KhKP2E2O3vvR2H58q9qpUxRGFspCduD
W0cL+/NBd168rag/nDugCGr0qUSrkqpNlL+fDDosfBHh8Qy0joDtnOiq4wdw0vMi
waUC2x6edw4GKAvC/elKtnK9WRH5NoWa5p9PY4H5i5AWXmii5IbMIHqr25nUvAL9
sS7NcdOuFvR05k9dce8kroj0UHxdU2LrcMGGpzS770hXjkZSrCGtzN2WZQARAQAB
tBtHaXRMYWIgQ1ZFIDxjdmVAZ2l0bGFiLmNvbT6JAlQEEwEKAD4WIQSN7uA0VTv5
j3YMqdL6be/LIZJiyAUCXrneqAIbAwUJAeEzgAULCQgHAgYVCgkICwIEFgIDAQIe
AQIXgAAKCRD6be/LIZJiyHosD/91MwTEl36TLMxdJqMVipPlwIFDFPUNoxn/OwBQ
GxdVm5WtbJhMaSHd5TMZUvhKMYqUpFNVmvESxwn5q8Tc74ZcADzhAsWK8z5xbDPD
l6VhPWO6aESrlUGLNrpzJCT7Edy6fKX4t5tYe3JRcZ2EpjgC1NlHzZbvo+sCwEb+
KhxlOXLrqkzuzs/MIerjy4v3heifcVVtZnP1Lps6Gl9PhyXXuHoXXhqsGgepJyJO
O+4SYo0WfocqeE23PFfZ0+RNHnOtvshb1saoo2nUl+u4MxqskUHSbWRS0rHeEJuj
YwEIiK/uCNkTqzuPzV7zD6lGJdpzopmgL3Po6WoWsM4vTt4WstzLwy2VUFJtMgqr
uMJYUw+G+UfHN5+YTM7+PV70kvlxvZ3Kj/iOqGx2BBWNeQBiC/TWhZxc7uRj5fHg
qpM1omHghmOWCh9q/GzfP6qSl1f2Fri+4qcHutciBWWgV7qfTQWa4f/h4PbuTbaa
KjZ6ITFGYsEfZSwQAClyKPN5QhJ5AF4csYRQOOr4+MLh3E3R6AvrjD0cdt7ewdCx
FSTwDm7YVCF2Sd36Bu8BFn5IxyayZtq1sRC93EnWPHyl8HiHBiV7fhOQhvi+J0bu
ca1HibqtI4cLXzx5b57oLxAZGIedyTJeMSt+apJd/G+j4qA02gUoedpMO7KLYJoa
44TWx7kCDQReud6oARAAx52wVpt9TzMbFOWzL+/yPjn01BlrssbQ/XZsDPUzgZGL
cRy55AtivBPhFl83KvgDXJaEmpwf5eA17jk3Az617Uz2MPTbXIVKddTS2d2DTsBd
dCsw7uAMjv6naJVSLVwFcPsgkpYSaWcegFjmOfBBFZnF+WL45yqFLk1E/Ek67ciB
/cQ32pPgce5+p/QZACyzWnlWrzPCyJJYKItFb160lXajpKLgT5X9b7dQ8M1G7oTw
rEaafxT3Mt+ICSlfWFZqd+cjLGswEfTtKF9jJJ/3GJw8MKgW3NFjEqt1U6HVNfZf
PfWOBe3pxlh9yS3d+1DGX/ornZKB3w1F7z4IUQAiMJZM+8VBQRYvrjLuyGlwhjTd
AiDzSDSvbNixP/BMdoZ//bZgej6FU711wqwMPqlIQS6WbyBrE6LmXjll/mahjgF0
jhDuBqgY8Sw3qLzcIWD1LGATeKVdNLpbVoUWwE9+J9XMNanVAtEZqD7AYxu24pVN
eoMLdrrfqt+HoLl8ziy3Ib3DYeNP/TkZH8talxJtq5wZ9E74PRtxk0XkVBjUEnFV
TjwYIkuOde1MJ8Xy1tzGSz0jFGP6ivom/DeK2Cm/hOroc8KW7s3HhSI+XJ7oclWU
buIzgkMtz5op9xofQ05lMw4kymghl9F5TvZm8AUd0H5GwWEv+SEF8JzeZ1m4WwMA
EQEAAYkCPAQYAQoAJhYhBI3u4DRVO/mPdgyp0vpt78shkmLIBQJeud6oAhsMBQkB
4TOAAAoJEPpt78shkmLI3wEQAJNHo9dBQNOX2/ehKWkwADdHFf48zwHvQCEK+KW6
Yl7xTRadIESzenRp+x5vCnwLe52FAjtHrW2q856/PQT3xQUHx26txsGIBFwCijrp
/jM32nM5scYI8yzu7GAteJuZC+FXWdHqzPjSJyf7fI4IEY3fNfnuusBUFKIiVupY
OP+PrZsZl8HwuL3Ojp9O/TWWzg7z/aA/JbL8JYz7RpLfk5pPCt+2yzXvoLCaIv+0
p6a8lm74i7fXtYdzK83wS4tT2AcPNGA4pGtOpSs8I5O0FxTGNiMXqV4qkt3VUgOn
qlJ1lXskM5BmyjajyWcyzn484kY+IQ4kjnli6ZNWHBGdVF9RCL8PIIttZsL2D6TS
mPhQSVdlWiMTexM1BxEtHbS5Q4G+hkhuU80bP4iwbz3sSO8+v+expI9CIRFrmZMI
2hwfpqKspK/z9meg4olnjUMOTU4OXu2TNwfUuYFJcwJ4IrDjAguKJXSa4fVTuh8W
e+JvhzpPqj0olU9TqeCSt79BZtm+nX352lOrl4YjwKY52oN9BNPDK/lf4u6HFkCw
s0YiVJ+Ch0vKVuWJaXKBx8zuHkZAc1/ADV9pIYZo1psJzJiONhQrwX+QsyWVDMFx
x07fbuhepzlhXvHT5GW5wTvRSnaDRUaYA2QTxLJTvUGSg9cOY1SGvdIvgVFVTPUO
C7kX
=LSYR
-----END PGP PUBLIC KEY BLOCK-----
```

[](){: name="external"}
## Disclosure Guidelines for Vulnerabilities in 3rd Party Software

When a security vulnerability in some 3rd party product is discovered by GitLab
team members the following disclosure guideline should apply:

* Our priority is to get the reported vulnerability fixed.
* If the 3rd party acknowledges the vulnerability and is working on a patch, we will keep vulnerability details confidential until the issue is fixed.
  * If possible, we will verify the fix before it is being published.
  * In special cases we might release details without a fix to make the public aware. This might, for instance, be the case when a vulnerability is being actively exploited.
* We aim for a fix within a 90 days deadline.
  * We will treat this as a soft deadline and help to meet the deadline when reporting.
  * We will try to coordinate with the affected 3rd party to have a patch released before we release an advisory.
* Resulting advisories will be published in the [disclosures repository](https://gitlab.com/gitlab-com/gl-security/disclosures).
