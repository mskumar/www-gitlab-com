# rubocop:disable Style/RegexpLiteral

monorepo_root = File.expand_path('../..', __dir__)

#--------------------------------------------------
# Only include this site's resources in this config
#--------------------------------------------------

class OnlyHandbookResources < Middleman::Extension
  self.resource_list_manipulator_priority = 1000

  def manipulate_resource_list(resources)
    resources.select do |resource|
      handbook_destination_path_regexes = [
        %r{^handbook/index.html},
        %r{^handbook/engineering},
        %r{^handbook/marketing},
        %r{^sitemap.xml}
      ]
      handbook_destination_path_regexes.any? { |regex| resource.destination_path =~ regex }
    end
  end
end
::Middleman::Extensions.register(:only_handbook_resources, OnlyHandbookResources)
activate :only_handbook_resources

#-----------------------------------------------------------------------------
# Make common files used by this site available to this site's Middleman build
#-----------------------------------------------------------------------------

set :layout, 'layout' # For some reason this has to be made explicit in the sub-site build, even though it's the default
set :data_dir, "#{monorepo_root}/data"
set :helpers_dir, "../../helpers" # This has to be relative, because Middleman's ExternalHelpers#after_configuration uses File.join(app.root, ...)

# The SourceWatcher which use `duplicate_relative_prefix` rely on middleman_source_watcher_find_monkeypatch.rb
# See comments in that file for details.
require_relative '../../extensions/middleman_source_watcher_find_monkeypatch'

files.watch(
  :source,
  path: File.expand_path("#{monorepo_root}/source", __dir__),
  only: [
    %r{sitemap.xml.builder}
  ]
)

# This is an explicit allow-list of common includes.  We do not include everything under the top level
# `/source/includes`, to increase performance, and to discourage adding site-specific files to the common files.
# Do not add a new entry here unless it is actually "common", and is used by multiple sites.
# Otherwise, add it under `/sites/handbook/source/includes`. For example:
#    Site-specific include file: `/sites/handbook/source/includes/example_include.html.haml`,
#    Partial reference from handbook template: `= partial "handbook/includes/example_include"`
common_includes = [
  %r{cc-license.html.haml},
  %r{form-to-resource.html.haml},
  %r{google/gtm.html.haml},
  %r{highlight_tooltip.*.html.erb},
  %r{hiring-chart.html.erb},
  %r{home/customer-logos-transparent.html.haml},
  %r{layout/footer-social-media.html.haml},
  %r{layout/footer.html.haml},
  %r{layout/handbook_search.html.haml},
  %r{layout/head.html.haml},
  %r{layout/header.html.haml},
  %r{layout/headline.html.haml},
  %r{layout/meta.html.haml},
  %r{layout/nav.html.haml},
  %r{layout/scripts.html.haml},
  %r{layout/search.html.haml},
  %r{logos/.*.svg},
  %r{master-prioritization-list.md},
  %r{performance_indicator.*.erb},
  %r{stages/tech-writing.html.haml},
  %r{team_member_list.haml},
  %r{team_member_table.haml},
  %r{ux-showcase/_schedule.md}
]
files.watch(
  :source,
  path: File.expand_path("#{monorepo_root}/source/includes", __dir__),
  duplicate_relative_prefix: 'includes',
  # Don't include the `includes` prefix in `only:` entries, because we excluded it with duplicate_relative_prefix
  only: common_includes
)

files.watch(
  :source,
  path: File.expand_path("#{monorepo_root}/source/direction", __dir__),
  duplicate_relative_prefix: 'direction'
)

common_experiments = [
  %r{6469.*}
]
files.watch(
  :source,
  path: File.expand_path("#{monorepo_root}/source/experiments", __dir__),
  duplicate_relative_prefix: 'experiments',
  only: common_experiments
)

files.watch(
  :source,
  path: File.expand_path("#{monorepo_root}/source/layouts", __dir__),
  destination_dir: ".",
  only: [
    %r{default.haml},
    %r{handbook-page-toc.haml},
    %r{layout.haml},
    %r{markdown_page.haml}
  ]
)

# This `images` files.watch is needed when common files
# (e.g. `/source/includes/product/maturity_legend.html.haml`) refer to
# images as partials
# (e.g. `= partial "/images/maturity/planned.svg"`)
# TODO: Don't do this, just use it like a normal image
common_images = [
  %r{organizations/.*}
]
files.watch(
  :source,
  path: File.expand_path("#{monorepo_root}/source/images", __dir__),
  destination_dir: ".",
  duplicate_relative_prefix: 'images',
  only: common_images
)

#----------------------------
# End of common file handling
#----------------------------

#---------------------------------------
# Rest of middleman config for this site
#---------------------------------------

# hack around relative requires elsewhere in the common code by adding the monorepo root to the load path
$LOAD_PATH.unshift(monorepo_root)

require_relative "../../extensions/breadcrumbs"
require_relative "../../lib/homepage"
require 'lib/mermaid'
require 'lib/plantuml'

# Disable HAML warnings
# https://github.com/middleman/middleman/issues/2087#issuecomment-307502952
Haml::TempleEngine.disable_option_validator!

# Per-page layout changes:
#
# With no layout
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

set :haml, {
  format: :xhtml
}

activate :syntax, line_numbers: false

set :markdown_engine, :kramdown
set :markdown, tables: true, hard_wrap: false, input: 'GFM'

activate :autoprefixer do |config|
  config.browsers = ['last 2 versions', 'Explorer >= 9']
end

activate :breadcrumbs, wrapper: :li, separator: '', hide_home: true, convert_last: false

# Build-specific configuration
configure :build do
  set :build_dir, "#{monorepo_root}/public"

  # NOTE: Currently no css or js in individual sites, it should all be at top level

  # Mermaid diagrams don't render without line breaks
  activate :minify_html, preserve_line_breaks: true

  Kramdown::Converter::PlantUmlHtmlWrapper.plantuml_setup
end

# TODO: Add this back when rest of handbook besides /marketing and /engineering is moved to sites/handbook/source
# # Hiring chart pages
# Gitlab::Homepage::Team.new.unique_departments.merge!(company: 'Company').each do |slug, name|
#   proxy "/handbook/hiring/charts/#{slug}/index.html", "/handbook/hiring/charts/template.html", locals: { department: name }, ignore: true
# end

# Compensation Roadmaps
data.compensation_roadmaps.each do |compensation_roadmap|
  proxy "/handbook/engineering/compensation-roadmaps/#{compensation_roadmap.slug}/index.html", "/handbook/engineering/compensation-roadmaps/template.html", locals: {
    compensation_roadmap: compensation_roadmap
  }, ignore: true
end

# GitLab Projects
proxy '/handbook/engineering/projects/index.html',
      '/handbook/engineering/projects/template.html',
      locals: { team: Gitlab::Homepage::Team.new },
      ignore: true

# Don't include the following into the sitemap
ignore '**/.gitkeep'

# rubocop:enable Style/RegexpLiteral
